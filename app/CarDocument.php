<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarDocument extends Model
{
    //
    protected $table = 'car_document';

    public function car()
    {
    	return $this->belongsTo('App\Car', 'car_id');
    }
}
