<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    //
    protected $table = 'cars';
    protected $primaryKey = 'id';

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function carDocument()
    {
    	return $this->hasMany('App\CarDocument', 'car_id', 'id');
    }

    public function tarif()
    {
        return $this->hasMany('App\Tarif', 'car_id', 'id');
    }
}
