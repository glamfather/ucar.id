<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPrice extends Model
{
    //
    protected $table = 'car_price';
    protected $primaryKey = 'id';

    public function car() {
        return $this->belongsTo('\App\Car', 'car_id');
    }

    public function paket() {
        return $this->belongsTo('\App\Paket', 'paket_id');
    }
}
