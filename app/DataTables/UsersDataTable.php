<?php

namespace App\DataTables;

use App\User;
use Yajra\Datatables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        $user = $this->query();
        return $this->datatables
            ->eloquent($user)
            ->addColumn('action', function($user){
                return '
                    <div class="btn-group btn-group-xs">
                        <form method="post" action="/admin/user/' . $user->id . '/delete">
                            ' . csrf_field() . '
                            <button type="submit" onclick="return confirm(\'Anda akan menghapus ' . $user->name . '\')" class="btn btn-xs btn-default" onclick="return confirm(\'Hapus?\')" style="color:red;"><i class="fa fa-trash"></i></button>
                        <a href="/admin/user/edit/' . $user->id . '" class="btn btn-xs btn-default" style="color:green;"><i class="fa fa-pencil"></i></a>
                        <a href="/admin/user/' . $user->id . '" class="btn  btn-xs btn-default" style="color:blue;"><i class="fa fa-eye"></i></a>
                        </form>
                    </div>
                    ';
            })
            ->editColumn('type', function($user){
                return ($user->type == 'user') ? '<i class="fa fa-user"></i> &nbsp;User' : '<i class="fa fa-star" style="color:orange;"></i> &nbsp;Admin';
            })
            ->editColumn('verified', function($user){
                return ($user->verified == 0) ? '<i class="fa fa-clock-o"></i> &nbsp;Unverified' : '<i class="fa fa-check" style="color:green;"></i> &nbsp;Verified';
            })
            ->editColumn('approved', function($user){
                return ($user->approved == 0) ? '<i class="fa fa-times-circle" style="color:orange;"></i> &nbsp;Unapproved' : '<i class="fa fa-check" style="color:green;"></i> &nbsp;Approved';
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::where('deleted','0');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    // ->addColumn(['title' => 'Email Verivication'])
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'email',
            'type',
            'approved',
            'verified',
            // 'id',
            // add your columns
            // 'created_at',
            // 'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatables_' . time();
    }
}
