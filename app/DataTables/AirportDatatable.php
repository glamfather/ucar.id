<?php

namespace App\DataTables;

use App\Airport;
use Yajra\Datatables\Services\DataTable;

class AirportDatatable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        $airport = $this->query();
        return $this->datatables
            ->eloquent($airport)
            ->addColumn('action', function($airport){
                return '
                    <div class="btn-group btn-group-xs">
                        <a href="/admin/airport/delete/' . $airport->id . '" class="btn btn-default" onclick="return confirm(\'Hapus?\')" style="color:red;"><i class="fa fa-trash"></i></a>
                        <a href="/admin/airport/edit/' . $airport->id . '" class="btn btn-default" style="color:green;"><i class="fa fa-pencil"></i></a>
                        <a href="/admin/airport/' . $airport->id . '" class="btn  btn-default" style="color:blue;"><i class="fa fa-eye"></i></a>
                    </div>
                    ';
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Airport::query();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addColumn(['data' => 'lat', 'title' => 'Latitude'])
                    ->addColumn(['data' => 'lon', 'title' => 'Longitude'])
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'code',
            'name',
            'cityCode',
            'cityName',
            // add your columns
            // 'created_at',
            // 'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'airportdatatables_' . time();
    }
}
