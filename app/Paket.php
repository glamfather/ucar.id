<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    //
    protected $table = 'paket';

    public function tarif() {
        return $this->hasMany('App\Tarif', 'paket_id', 'id');
    }
}
