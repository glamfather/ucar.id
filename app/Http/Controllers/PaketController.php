<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;

use App\Paket;

class PaketController extends Controller
{
    //
    public function index()
    {
    	$paket = Paket::all();
    	return view('admin.paket.index', compact('paket'));
    }

    public function create()
    {
        $paket = Paket::where('jenis', 'grab_my_car')->get();
    	return view('admin.paket.create', compact('paket'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'hour' => 'required|integer|min:1',
    		'range' => 'required|integer|min:1',
    		'keterangan' => 'required'
    	]);

    	$paket = new Paket();
    	$paket->jam = $request->hour;
    	$paket->jarak = $request->range;
    	$paket->keterangan = $request->keterangan;

    	$paket->save();

        Session::flash('message', 'Berhasil disimpan'); 
        Session::flash('alert-class', 'alert-success');
        
        return redirect('admin/paket');
    }

    public function edit($id, Request $request)
    {
    	$paket = Paket::findOrFail($id);

    	return view('admin.paket.edit', compact('paket'));
    }

    public function update($id, Request $request)
    {
    	$paket = Paket::findOrFail($id);
    	
    	// $this->validate($request, [
    	// 	'hour' => 'required|integer|min:1',
    	// 	'range' => 'required|integer|min:1',
    	// 	'keterangan' => 'required'
    	// ]);
    	
    	$paket->hour = $request->hour;
    	$paket->range = $request->range;
    	$paket->keterangan = $request->keterangan;

    	$paket->save();

        Session::flash('message', 'Berhasil disimpan'); 
        Session::flash('alert-class', 'alert-success');
        
        return redirect('admin/paket');
    }

    public function updateBatch(Request $request)
    {

        $this->validate($request, [
            'tempo' => 'array'
        ]);

        if (is_array($request->tempo)) {
            foreach ($request->tempo as $key => $value) {
                $paket = Paket::where('id', '!=', $key)->where('jenis', 'grab_my_car')->update(['is_active' => 'n']);
            }
        }

        Session::flash('message', 'Berhasil disimpan'); 
        Session::flash('alert-class', 'alert-success');
        
        return redirect('admin/paket');
    }
}
