<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FrontEndController extends Controller
{

    public function search()
    {
        //Cek tipe pencarian
        $t = ['j', 'n', 'g'];
        if (! in_array(request('t'), $t)){
            die('Kamu jelek...');
        }

        //Masing-masing fungsi pencarian dibedakan
        switch (request('t')) {
            case 'j' :
                $data = $this->jemput_search();
                break;
            case 'n' :
                $data = $this->normal_search();
                break;
            default:
                die('400. Bad request :(');
                break;
        }

        //Response mengambil hasil kembalian dari masing-masing fungsi pencarian
        return response()->json($data, 200);
    }

    public function jemput_search() {
        //Ambil airport berdasarkan kode yang dikirim GET
        $airport = \App\Airport::where('code', request('airport'))->first();
        if (! $airport) {
            die('Bandara tidak ditemukan');
        }

        //Definisi maksimum dan minimum harga tarif
        $maxTarif = request('maks');
        if (!request('maks') OR request('maks') <= 0) {
            $maxTarif = \App\Tarif::selectRaw('max(price) as maks')->first()->maks;
        }
        $minTarif = request('min');
        if (!request('min') OR request('min') <= 0) {
            $minTarif = \App\Tarif::selectRaw('min(price) as min')->first()->min;
        }

        //Query Utama
        $tarif = \App\Tarif::where('airport_id', $airport->id)
                ->where('price', '<=', $maxTarif)
                ->where('price', '>=', $minTarif)
                ->get();
        if (count($tarif) == 0) {
            die('Belum ada mobil di sekitar bandara ' . $airport->name);
        }

        //Binding hasil query ke dalam array
        foreach ($tarif as $k => $v) {
            //Mobil
            $data['mobil'][$k]['id']            = $v->car->id;
            $data['mobil'][$k]['vendor']        = $v->car->vendor;
            $data['mobil'][$k]['tipe']          = $v->car->tipe;
            $data['mobil'][$k]['bahan_bakar']   = $v->car->fuel;
            $data['mobil'][$k]['transmisi']     = $v->car->transmission_type;
            $data['mobil'][$k]['nomor_polisi']  = $v->car->police_number;
            $data['mobil'][$k]['keterangan']    = $v->car->keterangan;

            //Photo mobil
            foreach ($v->car->carDocument->where('document_type', 'photo') as $key => $item) {
                $data['mobil'][$k]['photo'][$key] = asset('uploads/img/carsdocument/' . $item->file_name);
            }

            //Tarif
            $data['mobil'][$k]['tarif']['harga']        = $v->price;
            $data['mobil'][$k]['tarif']['sopir']        = $v->sopir;
            $data['mobil'][$k]['tarif']['bbm']          = $v->bbm;
            $data['mobil'][$k]['tarif']['parkir']       = $v->parkir;
            $data['mobil'][$k]['tarif']['pp']           = $v->pp;
            $data['mobil'][$k]['tarif']['keterangan']   = $v->keterangan;

            //Pemilik Mobil
            $data['mobil'][$k]['pemilik']['id']     = $v->car->user->id;
            $data['mobil'][$k]['pemilik']['nama']   = $v->car->user->name;
            $data['mobil'][$k]['pemilik']['phone']  = $v->car->user->phone1;
            $data['mobil'][$k]['pemilik']['photo']  = asset('uploads/img/userphoto/' . $v->car->user->photo);
        }

        return $data;
    }

    public function normal_search() {
        $data = [];
        $lat = request('ljLat');
        $lng = request('ljLng');

        if (!$lat OR !$lng) {
            die('Longitude/Latitude osong');
        }

        //Definisi maksimum dan minimum harga tarif
        $maxTarif = request('maks');
        if (!request('maks') OR request('maks') <= 0) {
            $maxTarif = \App\Tarif::selectRaw('max(price) as maks')->first()->maks;
        }
        $minTarif = request('min');
        if (!request('min') OR request('min') <= 0) {
            $minTarif = \App\Tarif::selectRaw('min(price) as min')->first()->min;
        }

        //Query Utama
        $tarif = \App\Tarif::where('price', '<=', $maxTarif)
                ->where('price', '>=', $minTarif)
                ->get();

        // if (count($tarif) == 0) {
        //     die('Belum ada mobil di sekitar bandara ' . $airport->name);
        // }

        // dd($tarif[0]->car->user->longitude);
        //Binding hasil query ke dalam array
        foreach ($tarif as $k => $v) {
            if(\App\Library\CustomLibrary::vincentyGreatCircleDistance($lat, $lng, $v->car->user->latitude, $v->car->user->longitude) <= 50000) {
                //Mobil
                $data['mobil'][$k]['id']            = $v->car->id;
                $data['mobil'][$k]['vendor']        = $v->car->vendor;
                $data['mobil'][$k]['tipe']          = $v->car->tipe;
                $data['mobil'][$k]['bahan_bakar']   = $v->car->fuel;
                $data['mobil'][$k]['transmisi']     = $v->car->transmission_type;
                $data['mobil'][$k]['nomor_polisi']  = $v->car->police_number;
                $data['mobil'][$k]['keterangan']    = $v->car->keterangan;

                //Photo mobil
                foreach ($v->car->carDocument->where('document_type', 'photo') as $key => $item) {
                    $data['mobil'][$k]['photo'][$key] = asset('uploads/img/carsdocument/' . $item->file_name);
                }

                //Tarif
                $data['mobil'][$k]['tarif']['harga']        = $v->price;
                $data['mobil'][$k]['tarif']['sopir']        = $v->sopir;
                $data['mobil'][$k]['tarif']['bbm']          = $v->bbm;
                $data['mobil'][$k]['tarif']['parkir']       = $v->parkir;
                $data['mobil'][$k]['tarif']['pp']           = $v->pp;
                $data['mobil'][$k]['tarif']['keterangan']   = $v->keterangan;

                //Pemilik Mobil
                $data['mobil'][$k]['pemilik']['id']     = $v->car->user->id;
                $data['mobil'][$k]['pemilik']['nama']   = $v->car->user->name;
                $data['mobil'][$k]['pemilik']['phone']  = $v->car->user->phone1;
                $data['mobil'][$k]['pemilik']['photo']  = asset('uploads/img/userphoto/' . $v->car->user->photo);
            }
        }

        return $data;
    }
}