<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Indonesia;

class DaerahController extends Controller
{
    //
    public function allProvinces(){
        return response()->json(Indonesia::allProvinces(), 200);
    }

    public function provinceWithCities($province) {
        $data = '';
        $city = Indonesia::findProvince($province, ['cities']);
        foreach ($city->cities as $k => $v) {
            $data .= '<option value="' . $v->id . '">' . $v->name . '</option>';
        }
        return $data;
    }

    public function cityWithDistricts($city) {
        $data = '';
        $district = Indonesia::findCity($city, ['districts']);
        foreach ($district->districts as $k => $v) {
            $data .= '<option value="' . $v->id . '">' . $v->name . '</option>';
        }
        return $data;
    }

    public function districtWithVillages($district) {
        $data = '';
        $villages = Indonesia::findDistrict($district, ['villages']);
        foreach ($villages->villages as $k => $v) {
            $data .= '<option value="' . $v->id . '">' . $v->name . '</option>';
        }
        return $data;
    }
}
