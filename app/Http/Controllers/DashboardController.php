<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Airport;

class DashboardController extends Controller
{
    //
    public function index()
    {
    	$airport = Airport::count();
    	$user = User::count();

    	return view('admin.dashboard')->with(['airport' => $airport, 'user' => $user]);
    }
}
