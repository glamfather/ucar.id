<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\UsersDataTable;
use App\Http\Requests;
use Session;
use Input;
use Image;
use File;
use Auth;
use Hashids;
use Indonesia;

use App\User;


class UserController extends Controller
{
    //
    public function index(UsersDataTable $dataTable)
    {
    	return $dataTable->render('admin.users.index');
    }

    public function create()
    {
    	return view('admin.users.create');
    }

    public function show($id)
    {
    	$user = User::findOrFail($id);

    	return view('admin.users.detail',compact('user', $user));
    }

    public function store(Request $request)
    {

    	$this->validate($request,[
    		'name' => 'required|max:255',
    		'email' => 'required|email|unique:users,email',
    		'password' => 'required|max:255|confirmed',
            'photo' => 'sometimes|image'
            ]);

        $user = new User();
        $user->name      = $request->name;
        $user->email     = $request->email;
        $user->password  = bcrypt($request->password);

        if ($request->hasFile('photo')) {

            $intervention = Image::make($request->file('photo'));
            $intervention->fit(480,480 ,null,'top-left');

            $fileName = str_random(10) . '.png';
            $destinationPath = 'uploads/img/userphoto';
            $intervention->save($destinationPath.'/'.$fileName);

            $user->photo = $fileName;
        }

        $user->save();

        Session::flash('message', 'Berhasil disimpan'); 
        Session::flash('alert-class', 'alert-success');
        
        return redirect('admin/user');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit')->with('user', $user);
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);

        $rule = [
        'name' => 'required|max:255',
        'email' => 'required|email|unique:users,email,' . $user->id
        ];

        if ($request->password != null) {
            $rule['password'] = 'sometimes|required|confirmed';
        }

        $this->validate($request,$rule);

        $user->name      = $request->name;
        $user->email     = $request->email;
        if ($request->password != null) {
            $user->password  = bcrypt($request->password);
        }


        if ($request->hasFile('photo')) {
            if (file_exists('uploads/img/userphoto/' . $user->photo)) {
                if ($user->photo != 'default.png') {
                    File::delete('uploads/img/userphoto/' . $user->photo);
                }
            }

            $intervention = Image::make($request->file('photo'));
            $intervention->fit(480,480 ,null,'top-left');

            $fileName = str_random(10) . '.png';
            $destinationPath = 'uploads/img/userphoto';
            $intervention->save($destinationPath.'/'.$fileName);

            $user->photo = $fileName;
        }

        $user->save();

        Session::flash('message', 'Berhasil disimpan'); 
        Session::flash('alert-class', 'alert-success');
        
        return redirect('admin/user');

    }

    public function delete($id, Request $request)
    {
        //Cari User di Database
        $user = User::find($id);
        if (!$user) {
            abort(404);
        }

        //Hapus Gambar User
        if ($user->photo != 'default.png' && file_exists('uploads/img/userphoto/' . $user->photo)) {
            File::delete('uploads/img/userphoto/' . $user->photo);
        }

        $user->deleted = '1';
        $user->save();

        Session::flash('message', 'User Berhasil dihapus'); 
        Session::flash('alert-class', 'alert-success');
        
        return redirect('admin/user');
    }

    public function profile()
    {
        $user = Auth::user();
        return view('user.profile.index', compact('user'));
    }

    public function setting()
    {
        $user = Auth::user();
        $user_id = Hashids::encode($user->id);
        
//        dd($indonesia);
        return view('user.profile.setting', compact('user','user_id'));
    }

    public function settingSave(Request $request) {
        $user = User::find(Auth::user()->id);

        $rule = [
        'name' => 'required|max:255',
        'email' => 'required|email|unique:users,email,' . $user->id,
//        'provinsi' => 'required|integer',
//        'kabupaten' => 'required|integer',
//        'kecamatan' => 'required|integer',
//        'desa' => 'required|integer'
            'phone1' => 'required',
            'longitude' => 'required',
            'latitude' => 'required'
        ];

        if ($request->password != null) {
            $rule['password'] = 'sometimes|required|confirmed';
        }

        $this->validate($request,$rule);

        $user->name      = $request->name;
        $user->email     = $request->email;
//        $user->provinsi     = substr($request->desa, 0, 2);
//        $user->kabupaten     = substr($request->desa, 0, 4);
//        $user->kecamatan     = substr($request->desa, 0, 7);
//        $user->kelurahan     = $request->desa;
        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;
        $user->alamat     = $request->alamat;
        $user->phone1     = $request->phone1;
        $user->phone2     = $request->phone2;
        if ($request->password != null) {
            $user->password  = bcrypt($request->password);
        }


        if ($request->hasFile('photo')) {
            if (file_exists('uploads/img/userphoto/' . $user->photo)) {
                if ($user->photo != 'default.png') {
                    File::delete('uploads/img/userphoto/' . $user->photo);
                }
            }

            $intervention = Image::make($request->file('photo'));
            $intervention->fit(480,480 ,null,'top-left');

            $fileName = str_random(10) . '.png';
            $destinationPath = 'uploads/img/userphoto';
            $intervention->save($destinationPath.'/'.$fileName);

            $user->photo = $fileName;
        }

        $user->save();

        Session::flash('message', 'Berhasil disimpan');
        Session::flash('alert-class', 'alert-success');

        return redirect('profile/setting');
    }

    function isValidLatitude($latitude){
        if (preg_match("/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/", $latitude)) {
            return true;
        }
        
        return false;
    }

    function isValidLongitude($longitude){
        if(preg_match("/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}$/", $longitude)) {
            return true;
        }

        return false;
    }
}
