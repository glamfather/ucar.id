<?php

namespace App\Http\Controllers;

use App\Airport;
use App\Tarif;
use Hashids;
use Illuminate\Http\Request;
use App\DataTables\UsersDataTable;
use App\Http\Requests;
use Session;
use Input;
use Image;
use File;
use Auth;


use App\Car;
use App\CarDocument;
use App\User;
use App\Paket;

class CarController extends Controller
{

    public function index() {
        $mobil = Car::where('user_id', Auth::user()->id)->paginate(10);
        return view('user.mobil.mobilku', compact('mobil'));
    }

    public function tambah() {
        $paket = Paket::all();
        return view('user.mobil.tambahMobil', compact('paket'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'vendor' => 'required',
            'tipe' => 'required',
            'police_number' => 'required',
            'transmission_type' => 'required|in:matic,manual',
            'bbm' => 'sometimes|in:bbm,non_bbm',
            'gambar_utama' => 'required|image',
            'gambar' => 'sometimes|array'
        ]);

        $car = new Car();
        $car->user_id = Auth::user()->id;
        $car->vendor = $request->vendor;
        $car->tipe = $request->tipe;
        $car->year = $request->tahun;
        $car->police_number = $request->police_number;
        $car->transmission_type = $request->transmission_type;
        $car->fuel = $request->bbm;
        $car->keterangan = $request->keterangan;
        $car->save();

        if ($request->hasFile('gambar_utama')) {
            $intervention = Image::make($request->file('gambar_utama'));
            $intervention->fit(480,480 ,null,'center');

            $fileName = str_random(10) . '.png';
            $destinationPath = 'uploads/img/carsdocument';
            $intervention->save($destinationPath.'/'.$fileName);

            $carDocument = new CarDocument();
            $carDocument->user_id = Auth::user()->id;
            $carDocument->car_id = $car->id;
            $carDocument->document_type = 'photo';
            $carDocument->file_name = $fileName;
            $carDocument->save();
        }

        if(is_array($request->gambar)) {
            foreach ($request->gambar as $k => $v) {
                $intervention = Image::make($request->file('gambar_utama'));
                $intervention->fit(480, 480, null, 'center');

                $fileName = str_random(10) . '.png';
                $destinationPath = 'uploads/img/carsdocument';
                $intervention->save($destinationPath . '/' . $fileName);

                $carDocument = new CarDocument();
                $carDocument->user_id = Auth::user()->id;
                $carDocument->car_id = $car->id;
                $carDocument->document_type = 'photo';
                $carDocument->file_name = $fileName;
                $carDocument->save();
            }
        }

        Session::flash('message', 'Berhasil menambah mobil');
        Session::flash('alert-class', 'alert-success');

        return redirect('mobilku');
    }

    public function tarif(Request $request) {
        $mobil = Car::where('user_id', Auth::user()->id)->orderBy('vendor', 'ASC')->get();
        $paket = Paket::where('is_active', 'y')->get();
        $airports = Airport::orderBy('name', 'ASC')->get();

        $tarif = null;
        if ($mobil) {
            foreach ($mobil as $k => $v) {
                $mobil_id[$k] = $v->id;
            }
            $tarif = \App\Tarif::whereIn('car_id', $mobil_id)->orderBy('id', 'DESC')->paginate(3);
        }

        //Cek distance between airport and user location
        $airport = null;
        if ($airports){
            foreach ($airports as $k => $v) {
                if(\App\Library\CustomLibrary::vincentyGreatCircleDistance($v->lat, $v->lon, Auth::user()->latitude, Auth::user()->longitude) <= 50000) {
                    $airport[$k]['id'] = $v->id;
                    $airport[$k]['name'] = $v->name;
                    $airport[$k]['code'] = $v->code;
                }
            }
        }

        return view('user.mobil.tarif', compact('mobil', 'paket', 'airport', 'tarif'));
    }

    public function tarifLama($id, Request $request){
        //Mobil valid
        $id = Hashids::connection('mobil')->decode($id);

        $mobil = Car::find((isset($id[0])) ? $id[0] : 0);
        
        //User = pemilik mobil??
        if (!$mobil OR $mobil->user_id != Auth::user()->id) {
            abort(404);
        }
        
        $paket = \App\Paket::all();

        return view('user.mobil.detail', compact('mobil', 'paket'));
    }

    public function tarifStore(Request $request) {
        //Mobil valid?
        $mobil_id = Hashids::connection('mobil')->decode($request->mobil);
        $mobil = Car::find((isset($mobil_id[0])) ? $mobil_id[0] : 0);

        //User = pemilik mobil??
        if (!$mobil OR $mobil->user_id != Auth::user()->id) {
            abort(400);
        }

        //Validation
        $this->validate($request, [
            'mobil' => 'required',
            'price' => 'required|integer|min:1',
            'paket' => 'required',

        ]);

        //Cek paket
        $paket = \App\Paket::find(Hashids::connection('paket')->decode($request->paket));

        // Validate Paket
        if (!$paket) {
            abort(400);
        }


        //Check existing paket. If present, then update it. Otherwise, Create it.
        $tarif = \App\Tarif::where('car_id', $mobil->id)
            ->where('paket_id', $paket[0]->id)
            ->first();
        if(!$tarif) {
            $tarif = new \App\Tarif();
        }
        $tarif->car_id = $mobil->id;
        $tarif->paket_id = $paket[0]->id;
        $tarif->price = $request->price;

        //Cek Bandara
        if ($request->bandara) {
            $bandara = \App\Airport::find(Hashids::connection('airport')->decode($request->bandara));
            if (!$bandara) {
                die('Bandara tidak valid');
            }
            $tarif->airport_id = $bandara[0]->id;
            $tarif->latitude = $bandara[0]->lat;
            $tarif->longitude = $bandara[0]->lon;
        }

        $tarif->sopir = ($request->sopir) ? 1 : 0;
        $tarif->bbm = ($request->bbm) ? 1 : 0;
        $tarif->tol = ($request->tol) ? 1 : 0;
        $tarif->parkir = ($request->parkir) ? 1 : 0;
        $tarif->pp = ($request->pp) ? 1 : 0;
        $tarif->keterangan = $request->keterangan;
        $tarif->save();

        Session::flash('message', 'Berhasil menyimpan tarif');
        Session::flash('alert-class', 'alert-success');

        //Everything is OK
        return back();
    }

    public function detail($id, Request $request)
    {
        $mobil_id = Hashids::connection('mobil')->decode($id);
        $mobil = Car::find((isset($mobil_id[0])) ? $mobil_id[0] : 0);

        //User = pemilik mobil??
        if (!$mobil OR $mobil->user_id != Auth::user()->id) {
            abort(400);
        }

        $tarif = \App\Tarif::where('car_id', $mobil->id)->get();

        return view('user.mobil.detail', compact('mobil', 'tarif'));
    }

}
