<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\UsersDataTable;
use App\Http\Requests;
use Session;
use Input;
use Image;
use File;
use Auth;


use App\UserDocument;
use \App\User;

class DokumenkuController extends Controller
{
    //
    public function index() {
        $dokumen = UserDocument::where('user_id', Auth::user()->id)->get();
        return view('user.dokumen.index', compact('dokumen'));
    }

    public function tambah(){
        return view('user.dokumen.tambah');
    }

    public function store(Request $request) {
        $user = Auth::user();

        $rule = [
            'ktp' => 'required|image',
            'skck' => 'required|image',
            'sim' => 'required|image',
            'kk' => 'required|image'
        ];

        $this->validate($request,$rule);

        if ($request->hasFile('ktp')) {
            $oldKtp = UserDocument::where('user_id', $user->id)->where('document_type', 'ktp')->first();
            if ($oldKtp AND file_exists('uploads/img/userdocument/' . $oldKtp->file_name)) {
                File::delete('uploads/img/userdocument/' . $oldKtp->file_name);
                $ktp = $oldKtp;
            }else{
                $ktp = new UserDocument();
            }

            $intervention = Image::make($request->file('ktp'));

            $fileName = str_random(10) . '.png';
            $destinationPath = 'uploads/img/userdocument';
            $intervention->save($destinationPath.'/'.$fileName);
            $ktp->document_type = 'ktp';
            $ktp->user_id = $user->id;
            $ktp->file_name = $fileName;
            $ktp->save();
        }

        if ($request->hasFile('kk')) {
            $oldKk = UserDocument::where('user_id', $user->id)->where('document_type', 'kk')->first();
            if ($oldKk AND file_exists('uploads/img/userdocument/' . $oldKk->file_name)) {
                File::delete('uploads/img/userdocument/' . $oldKk->file_name);
                $kk = $oldKk;
            }else{
                $kk = new UserDocument();
            }

            $intervention = Image::make($request->file('kk'));

            $fileName = str_random(10) . '.png';
            $destinationPath = 'uploads/img/userdocument';
            $intervention->save($destinationPath.'/'.$fileName);
            $kk->document_type = 'kk';
            $kk->user_id = $user->id;
            $kk->file_name = $fileName;
            $kk->save();
        }

        if ($request->hasFile('skck')) {
            $oldSkck = UserDocument::where('user_id', $user->id)->where('document_type', 'skck')->first();
            if ($oldSkck AND file_exists('uploads/img/userdocument/' . $oldSkck->file_name)) {
                File::delete('uploads/img/userdocument/' . $oldSkck->file_name);
                $skck = $oldSkck;
            }else {
                $skck = new UserDocument();
            }

            $intervention = Image::make($request->file('skck'));

            $fileName = str_random(10) . '.png';
            $destinationPath = 'uploads/img/userdocument';
            $intervention->save($destinationPath.'/'.$fileName);
            $skck->document_type = 'skck';
            $skck->user_id = $user->id;
            $skck->file_name = $fileName;
            $skck->save();
        }

        if ($request->hasFile('sim')) {
            $oldSim = UserDocument::where('user_id', $user->id)->where('document_type', 'sim')->first();
            if ($oldSim AND file_exists('uploads/img/userdocument/' . $oldSim->file_name)) {
                File::delete('uploads/img/userdocument/' . $oldSim->file_name);
                $sim = $oldSim;
            }else{
                $sim = new UserDocument();
            }

            $intervention = Image::make($request->file('sim'));

            $fileName = str_random(10) . '.png';
            $destinationPath = 'uploads/img/userdocument';
            $intervention->save($destinationPath.'/'.$fileName);
            $sim->document_type = 'sim';
            $sim->user_id = $user->id;
            $sim->file_name = $fileName;
            $sim->save();
        }

        Session::flash('message', 'Berhasil disimpan');
        Session::flash('alert-class', 'alert-success');

        return redirect('dokumenku');
    }
}
