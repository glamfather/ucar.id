<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\AirportDataTable;
use App\Airport;
use App\Http\Requests;
use Mapper;

class AirportController extends Controller
{
    //
    public function index(AirportDataTable $dataTable)
    {
        return $dataTable->render('admin.airports.index');
    }

    public function show($id)
    {
        $airport = Airport::find($id);
        $mapper = Mapper::map(53.381128999999990000, -1.470085000000040000, ['zoom' => 10, 'markers' => ['title' => 'My Location', 'animation' => 'DROP'], 'clusters' => ['size' => 10, 'center' => true, 'zoom' => 20]]);
        return view('admin.airports.detail', compact(['airport', 'mapper']));
    }

    public function search($keyword = null)
    {
        $data = [
            'status' => '0',
            'data' => [],
            'message' => ''
        ];
        if (!$keyword) {
            $airport = Airport::all();
        }else{
            $airport = Airport::where('code', 'like', '%' . $keyword . '%')
                            ->orWhere('name', 'like', '%' . $keyword . '%')
                            ->orWhere('cityCode', 'like', '%' . $keyword . '%')
                            ->orWhere('cityName', 'like', '%' . $keyword . '%')->get();
        }
        if ($airport) {
            foreach ($airport as $key => $value) {
                $data['data']['airport'][$key]['id'] = $value->id;
                $data['data']['airport'][$key]['code'] = $value->code;
                $data['data']['airport'][$key]['name'] = $value->name;
                $data['data']['airport'][$key]['cityCode'] = $value->cityCode;
                $data['data']['airport'][$key]['cityName'] = $value->cityName;
            }
        }

        echo trim(json_encode($data['data']), '"airport":{}');
    }

    public function genSeeder()
    {
    	$airport = Airport::where('countryCode', 'ID')->distinct('code')->get();
    	// dd($airport);
    	echo "<pre>";
    	foreach ($airport as $key => $value) {
    		echo "DB::insert('insert into airports (\n\tcode, 
            \tname, 
            \tcityCode, 
            \tcityName, 
            \tcountryName, 
            \tcountryCode, 
            \ttimezone, 
            \tlat, 
            \tlon, 
            \tnumAirports, 
            \tcity,
            \tcreated_at,
            \tupdated_at
            ) values (\n
            \t:code, 
            \t:name, 
            \t:cityCode, 
            \t:cityName, 
            \t:countryName, 
            \t:countryCode, 
            \t:timezone, 
            \t:lat, 
            \t:lon, 
            \t:numAirports, 
            \t:city,
            \t:created_at,
            \t:updated_at\n
            )', [\n";
            echo "\t'code'\t =>\t '" . $value->code . "'," . "\n";
            echo "\t'name'\t =>\t '" . $value->name . "'," . "\n";
            echo "\t'cityCode'\t =>\t '" . $value->cityCode . "'," . "\n";
            echo "\t'cityName'\t =>\t '" . $value->cityName . "'," . "\n";
            echo "\t'countryName'\t =>\t '" . $value->countryName . "'," . "\n";
            echo "\t'countryCode'\t =>\t '" . $value->countryCode . "'," . "\n";
            echo "\t'timezone'\t =>\t '" . $value->timezone . "'," . "\n";
            echo "\t'lat'\t =>\t '" . $value->lat . "'," . "\n";
            echo "\t'lon'\t =>\t '" . $value->lon . "'," . "\n";
            echo "\t'numAirports'\t =>\t '" . $value->numAirports . "'," . "\n";
            echo "\t'city'\t =>\t '" . $value->city . "'," . "\n";
            echo "\t'created_at'\t =>\t '2016-10-09 04:36:17'," . "\n";
            echo "\t'updated_at'\t =>\t '2016-10-09 04:36:17'" . "\n";
            echo "]);" . "\n\n\n";
        }
    }
}
