<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDocument extends Model
{
    //
    protected $table = 'user_document';

    public function user()
    {
    	return $this->belongsTo('App\User', 'id', 'user_id');
    }
}
