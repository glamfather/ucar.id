<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AirportsTableSeeder::class);
    }
}

class AirportsTableSeeder extends Seeder {
   
 public function run()
 {
         //delete airports table records
   DB::table('airports')->delete();

         //insert some dummy records
   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'AMQ',
    'name'   =>  'Pattimura Arpt',
    'cityCode'   =>  'AMQ',
    'cityName'   =>  'Ambon',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '9',
    'lat'    =>  '-3.710264',
    'lon'    =>  '128.089136',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'AMI',
    'name'   =>  'Selaparang Airport',
    'cityCode'   =>  'AMI',
    'cityName'   =>  'Mataram',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '8',
    'lat'    =>  '-8.560708',
    'lon'    =>  '116.094656',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'AHI',
    'name'   =>  'Amahai Airport',
    'cityCode'   =>  'AHI',
    'cityName'   =>  'Amahai',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '-100',
    'lat'    =>  '0',
    'lon'    =>  '0',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'BDJ',
    'name'   =>  'Sjamsudin Noor Arpt',
    'cityCode'   =>  'BDJ',
    'cityName'   =>  'Banjarmasin',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '8',
    'lat'    =>  '-3.442356',
    'lon'    =>  '114.762553',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'BDO',
    'name'   =>  'Husein Sastranegara Arpt',
    'cityCode'   =>  'BDO',
    'cityName'   =>  'Bandung',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-6.900625',
    'lon'    =>  '107.576294',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'BIK',
    'name'   =>  'Mokmer Arpt',
    'cityCode'   =>  'BIK',
    'cityName'   =>  'Biak',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '9',
    'lat'    =>  '-1.190017',
    'lon'    =>  '136.107997',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'BKS',
    'name'   =>  'Padangkemiling Arpt',
    'cityCode'   =>  'BKS',
    'cityName'   =>  'Bengkulu',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-3.8637',
    'lon'    =>  '102.339036',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'BPN',
    'name'   =>  'Sepingan Arpt',
    'cityCode'   =>  'BPN',
    'cityName'   =>  'Balikpapan',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '8',
    'lat'    =>  '-1.268272',
    'lon'    =>  '116.894478',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'BTH',
    'name'   =>  'Hang Nadim Arpt',
    'cityCode'   =>  'BTH',
    'cityName'   =>  'Batam',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '1.121028',
    'lon'    =>  '104.118753',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'BTJ',
    'name'   =>  'Blang Bintang Arpt',
    'cityCode'   =>  'BTJ',
    'cityName'   =>  'Banda Aceh',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '5.523522',
    'lon'    =>  '95.420372',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'CBN',
    'name'   =>  'Penggung Arpt',
    'cityCode'   =>  'CBN',
    'cityName'   =>  'Cirebon',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-6.756144',
    'lon'    =>  '108.539672',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'CXP',
    'name'   =>  'Tunggul Wulung Arpt',
    'cityCode'   =>  'CXP',
    'cityName'   =>  'Cilacap',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-7.645056',
    'lon'    =>  '109.033911',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'DJB',
    'name'   =>  'Sultan Taha Syarifudin Arpt',
    'cityCode'   =>  'DJB',
    'cityName'   =>  'Jambi',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-1.638017',
    'lon'    =>  '103.644378',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'DJJ',
    'name'   =>  'Sentani Arpt',
    'cityCode'   =>  'DJJ',
    'cityName'   =>  'Jayapura',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '9',
    'lat'    =>  '-2.576953',
    'lon'    =>  '140.516372',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'DPS',
    'name'   =>  'Ngurah Rai Arpt',
    'cityCode'   =>  'DPS',
    'cityName'   =>  'Denpasar Bali',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '8',
    'lat'    =>  '-8.748169',
    'lon'    =>  '115.167172',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'GNS',
    'name'   =>  'Gunungsitoli Arpt',
    'cityCode'   =>  'GNS',
    'cityName'   =>  'Gunungsitoli',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '1.166381',
    'lon'    =>  '97.704681',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'HLP',
    'name'   =>  'Halim Perdana Kusuma Arpt',
    'cityCode'   =>  'JKT',
    'cityName'   =>  'Jakarta',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-6.26661',
    'lon'    =>  '106.891',
    'numAirports'    =>  '2',
    'city'   =>  '',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'CGK',
    'name'   =>  'Soekarno Hatta Intl',
    'cityCode'   =>  'JKT',
    'cityName'   =>  'Jakarta',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-6.125567',
    'lon'    =>  '106.655897',
    'numAirports'    =>  '2',
    'city'   =>  '',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'JOG',
    'name'   =>  'Adisutjipto Arpt',
    'cityCode'   =>  'JOG',
    'cityName'   =>  'Yogyakarta',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-7.788181',
    'lon'    =>  '110.431758',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'MDC',
    'name'   =>  'Samratulang Arpt',
    'cityCode'   =>  'MDC',
    'cityName'   =>  'Manado',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '8',
    'lat'    =>  '1.549447',
    'lon'    =>  '124.925878',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'MES',
    'name'   =>  'Polonia Arpt',
    'cityCode'   =>  'MES',
    'cityName'   =>  'Medan',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '3.558056',
    'lon'    =>  '98.671722',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'MJU',
    'name'   =>  'Mamuju Arpt',
    'cityCode'   =>  'MJU',
    'cityName'   =>  'Mamuju',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '9',
    'lat'    =>  '-2.5',
    'lon'    =>  '118.833336',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'MKW',
    'name'   =>  'Rendani Arpt',
    'cityCode'   =>  'MKW',
    'cityName'   =>  'Manokwari',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '9',
    'lat'    =>  '-0.891833',
    'lon'    =>  '134.049183',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'MLG',
    'name'   =>  'Malang Arpt',
    'cityCode'   =>  'MLG',
    'cityName'   =>  'Malang',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-7.926556',
    'lon'    =>  '112.714514',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'NAM',
    'name'   =>  'Namlea Arpt',
    'cityCode'   =>  'NAM',
    'cityName'   =>  'Namlea',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '-100',
    'lat'    =>  '0',
    'lon'    =>  '0',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'PDG',
    'name'   =>  'Tabing Arpt',
    'cityCode'   =>  'PDG',
    'cityName'   =>  'Padang',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-0.874989',
    'lon'    =>  '100.351881',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'PKN',
    'name'   =>  'Pangkalanbuun Arpt',
    'cityCode'   =>  'PKN',
    'cityName'   =>  'Pangkalanbun',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-2.705197',
    'lon'    =>  '111.673208',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'PKU',
    'name'   =>  'Simpang Tiga Arpt',
    'cityCode'   =>  'PKU',
    'cityName'   =>  'Pekanbaru',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '0.460786',
    'lon'    =>  '101.444539',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'PLM',
    'name'   =>  'Mahmud Badaruddin Li Arpt',
    'cityCode'   =>  'PLM',
    'cityName'   =>  'Palembang',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-2.89825',
    'lon'    =>  '104.699903',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'PLW',
    'name'   =>  'Mutiara Arpt',
    'cityCode'   =>  'PLW',
    'cityName'   =>  'Palu',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '8',
    'lat'    =>  '-0.918542',
    'lon'    =>  '119.909642',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'PNK',
    'name'   =>  'Supadio International Arpt',
    'cityCode'   =>  'PNK',
    'cityName'   =>  'Pontianak',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-0.150711',
    'lon'    =>  '109.403892',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'SOC',
    'name'   =>  'Adi Sumarno Arpt',
    'cityCode'   =>  'SOC',
    'cityName'   =>  'Solo',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-7.516089',
    'lon'    =>  '110.756892',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'SOQ',
    'name'   =>  'Jefman Arpt',
    'cityCode'   =>  'SOQ',
    'cityName'   =>  'Sorong',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '9',
    'lat'    =>  '-0.926358',
    'lon'    =>  '131.121194',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'SRG',
    'name'   =>  'Achmad Uani Arpt',
    'cityCode'   =>  'SRG',
    'cityName'   =>  'Semarang',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-6.971447',
    'lon'    =>  '110.374122',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'SUB',
    'name'   =>  'Juanda Arpt',
    'cityCode'   =>  'SUB',
    'cityName'   =>  'Surabaya',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-7.379831',
    'lon'    =>  '112.786858',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'TIM',
    'name'   =>  'Timika Arpt',
    'cityCode'   =>  'TIM',
    'cityName'   =>  'Timika',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '9',
    'lat'    =>  '-4.528275',
    'lon'    =>  '136.887375',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'TJQ',
    'name'   =>  'Bulutumbang Arpt',
    'cityCode'   =>  'TJQ',
    'cityName'   =>  'Tanjung Pandan',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-2.745722',
    'lon'    =>  '107.754917',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'TKG',
    'name'   =>  'Branti Arpt',
    'cityCode'   =>  'TKG',
    'cityName'   =>  'Bandar Lampung',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '7',
    'lat'    =>  '-5.242339',
    'lon'    =>  '105.178939',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'TRK',
    'name'   =>  'Tarakan Arpt',
    'cityCode'   =>  'TRK',
    'cityName'   =>  'Tarakan',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '8',
    'lat'    =>  '3.326694',
    'lon'    =>  '117.565569',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);


   DB::insert('insert into airports (
    code, 
    name, 
    cityCode, 
    cityName, 
    countryName, 
    countryCode, 
    timezone, 
    lat, 
    lon, 
    numAirports, 
    city,
    created_at,
    updated_at
    ) values (

    :code, 
    :name, 
    :cityCode, 
    :cityName, 
    :countryName, 
    :countryCode, 
    :timezone, 
    :lat, 
    :lon, 
    :numAirports, 
    :city,
    :created_at,
    :updated_at

    )', [
    'code'   =>  'UPG',
    'name'   =>  'Hasanudin Arpt',
    'cityCode'   =>  'UPG',
    'cityName'   =>  'Ujung Pandang',
    'countryName'    =>  'INDONESIA',
    'countryCode'    =>  'ID',
    'timezone'   =>  '8',
    'lat'    =>  '-5.061631',
    'lon'    =>  '119.554042',
    'numAirports'    =>  '1',
    'city'   =>  'true',
    'created_at'     =>  '2016-10-09 04:36:17',
    'updated_at'     =>  '2016-10-09 04:36:17'
    ]);

}

}

