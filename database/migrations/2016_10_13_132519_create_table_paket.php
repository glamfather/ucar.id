<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('paket', function(Blueprint $table){
            $table->increments('id');
            $table->integer('jam')->nullable();
            $table->integer('jarak')->nullable();
            $table->enum('jenis', ['normal', 'jemput', 'grab_my_car'])->default('normal');
            $table->string('tempo')->nullable();
            $table->string('keterangan')->nullable();
            $table->enum('is_active', ['y', 'n'])->default('y');
            $table->timestamps();                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('paket');
    }
}
