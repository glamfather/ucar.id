<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id');
            $table->integer('owner_id');
            $table->integer('tenant_id');
            $table->enum('order_type', ['regular', 'taxy_online', 'to_airport', 'from_airport']);
            $table->string('start_point')->nullable();
            $table->string('destination')->nullable();
            $table->dateTime('start_time')->nullable();
            $table->text('information')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('order');
    }
}
