<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->enum('type', ['admin', 'user'])->default('user');
            $table->string('password');
            $table->string('photo')->default('default.png');
            $table->enum('verified', ['0', '1'])->default('0');
            $table->enum('approved', ['0', '1'])->default('0');
            $table->enum('deleted', ['0', '1'])->default('0');
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->string('alamat')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
