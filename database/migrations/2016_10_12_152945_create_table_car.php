<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('vendor');
            $table->string('tipe')->nullable();
            $table->string('year')->nullable();
            $table->string('police_number')->nullable();
            $table->enum('transmission_type', ['matic', 'manual']);
            $table->string('fuel');
            $table->string('keterangan')->nullable();
            $table->enum('status', ['paused', 'active', 'booked', 'on_demand'])->default('paused');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cars');
    }
}
