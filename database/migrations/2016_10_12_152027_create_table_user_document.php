<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserDocument extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('user_document', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->enum('document_type', ['ktp', 'sim', 'passport', 'passphoto', 'kk', 'surat_nikah', 'skck', 'app_screenshot','other']);
            $table->string('file_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('user_document');
    }
}
