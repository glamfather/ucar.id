<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/sendmail', function(\Illuminate\Http\Request $request, \Illuminate\Mail\Mailer $mail) {
    $mail->to($request->input('to'))
        ->send(new \App\Mail\MyMail($request->input('title')));
    // return redirect()->back();
})->name('sendmail');


Route::get('/', function () {
    // return view('welcome');
	return view('layouts.front-end');
});

Route::get('front', function(){
	return view('layouts.front-end');
});

Route::get('listing', function(){
    return view('listing');
});

Route::get('cari', function(){
    return view('listing');
});

//Daerah
Route::get('allProvinces', 'DaerahController@allProvinces');
Route::get('provinceWithCities/{province}', 'DaerahController@provinceWithCities');
Route::get('cityWithDistricts/{city}', 'DaerahController@cityWithDistricts');
Route::get('districtWithVillages/{district}', 'DaerahController@districtWithVillages');


//Airport Search
Route::get('airport/search/{keyword?}', 'AirportController@search');

//Search
Route::get('search', ['as' => 'search', 'uses'  => 'FrontEndController@search']);



/*
|--------------------------------------------------------------------------
| ADMIN ROUTES
|--------------------------------------------------------------------------
| Only admin can access these routes.
*/
Route::group(['middleware' => ['admin']], function(){
//Admin
    Route::get('admin', 		['as' => 'admin' , 'uses' => 'DashboardController@index']);

	//Dashboard
	Route::get('admin/dashboard', 		['as' => 'admin.dashboard' , 'uses' => 'DashboardController@index']);

	//User
	Route::get('admin/users',			[ 'as' => 'admin.users', 	'uses' => 	'UserController@index']);
	Route::get('admin/user',			[ 'as' => 'admin.user', 	'uses' => 	'UserController@index']);
	Route::get('admin/user/create',		[ 'as' => 'admin.user.create', 	'uses' => 	'UserController@create']);
	Route::post('admin/user/store',		[ 'as' => 'admin.user.post', 	'uses' => 	'UserController@store']);
	Route::get('admin/user/edit/{id}', 	[ 'as' => 'admin.user.edit',	'uses' =>  	'UserController@edit']);
	Route::post('admin/user/update/{id}', 	[ 'as' => 'admin.user.edit',	'uses' =>  	'UserController@update']);
	Route::post('admin/user/{id}/delete', 	[ 'as' => 'admin.user.delete',	'uses' =>  	'UserController@delete']);
	Route::get('admin/user/{id}', 		[ 'as' => 'admin.user.detail',	'uses' =>  	'UserController@show']);

	//Airport
	Route::get('admin/airports',			[ 'as' => 'admin.airports', 	'uses' => 	'AirportController@index']);
	Route::get('admin/airport',				[ 'as' => 'admin.airport', 	'uses' => 	'AirportController@index']);
	Route::get('admin/airport/create',		[ 'as' => 'admin.airport.create', 	'uses' => 	'AirportController@create']);
	Route::get('admin/airport/{id}', 		[ 'as' => 'admin.airport.detail',	'uses' =>  	'AirportController@show']);
	Route::post('admin/airport/store',		[ 'as' => 'admin.airport.post', 	'uses' => 	'AirportController@store']);

	//Paket
	Route::get('admin/paket',			[ 'as' => 'admin.paket', 	'uses' => 	'PaketController@index']);
	Route::get('admin/paket/create',			[ 'as' => 'admin.paket.create', 	'uses' => 	'PaketController@create']);
	Route::post('admin/paket/store',			[ 'as' => 'admin.paket.store', 	'uses' => 	'PaketController@store']);
	Route::get('admin/paket/edit/{id}',			[ 'as' => 'admin.paket.edit', 	'uses' => 	'PaketController@edit']);
	Route::post('admin/paket/update/batch',			[ 'as' => 'admin.paket.update.batch', 	'uses' => 	'PaketController@updateBatch']);
	Route::post('admin/paket/update/{id}',			[ 'as' => 'admin.paket.update', 	'uses' => 	'PaketController@update']);

});


/*
|--------------------------------------------------------------------------
| LOGGED-IN ROUTES
|--------------------------------------------------------------------------
| Only users who logged-in can access these routes.
*/
Route::group(['middleware' => ['auth']], function(){
    //Dashboard
	Route::get('dashboard', [ 'as' => 'dashboard', 'uses' => 'HomeController@index']);

    //Profile
    Route::get('/profile', ['as' => 'profile', 'uses' => 'UserController@profile']);
    Route::get('/profile/setting', ['as' => 'profile.setting', 'uses' => 'UserController@setting']);
    Route::post('/profile/setting', ['as' => 'setting.post', 'uses' => 'UserController@settingSave']);

    //Mobilku
    Route::get('mobilku', ['as' => 'mobilku.index', 'uses' => 'CarController@index']);
    Route::get('mobilku/detail/{id}', ['as' => 'mobilku.detail', 'uses' => 'CarController@detail']);
    Route::get('mobilku/tambah', ['as' => 'mobilku.tambah', 'uses' => 'CarController@tambah']);
    Route::post('mobilku/tambah', ['as' => 'mobilku.tambah', 'uses' => 'CarController@store']);
    Route::get('mobilku/tarif', ['as' => 'mobilku.tarif', 'uses' => 'CarController@tarif']);
    Route::get('mobilku/tarif/detail/{id}', ['as' => 'mobilku.tarif.detail', 'uses' => 'CarController@tarif']);
//    Route::get('mobilku/tarif/{id}', ['as' => 'mobilku.harga', 'uses' => 'CarController@tarif']);
    Route::post('mobilku/tarif', ['as' => 'mobilku.harga', 'uses' => 'CarController@tarifStore']);

    //Transaksi
	Route::get('transaksi', ['as' => 'transaksi', function(){
		return view('user.transaksi.index');
	}]);

    //Inbox
	Route::get('inbox', ['as' => 'inbox', function(){
		return view('user.inbox.index');
	}]);

    //Dokumenku
	Route::get('dokumenku', ['as' => 'dokumenku.index', 'uses' => 'DokumenkuController@index']);
    Route::get('dokumenku/tambah', ['as' => 'dokumenku.tambah', 'uses' => 'DokumenkuController@tambah']);
    Route::post('dokumenku/tambah', ['as' => 'dokumenku.tambah', 'uses' => 'DokumenkuController@store']);

});
