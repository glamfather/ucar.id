<?php

/*
 * This file is part of Laravel Hashids.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Hashids Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'salt' => 'qwoeihckjzxc87236',
            'length' => 6,
            'alphabet' => 'BisMlLAhmHdD19iKw',
        ],

        'mobil' => [
            'salt' => 'AlhamdulillahiRobbilAlamin',
            'length' => 6,
            'alphabet' => 'mMoObBbiIlL0987654321',
        ],

        'paket' => [
            'salt' => 'MalikiYaumiddin',
            'length' => 4,
            'alphabet' => 'pPaAkKeT0987654321',
        ],

        'airport' => [
            'salt' => 'IyyakaNabuduWaiyyakaNastain',
            'length' => 4,
            'alphabet' => 'aAiIrRpPoOtT0987654321',
        ],

        'order' => [
            'salt' => 'Bismillahirrohmanirrohim',
            'length' => 6,
            'alphabet' => 'uUcCaArRiIdD1234567890',
        ],

        'tarif' => [
            'salt' => 'IhdinashhirothalMustaqim',
            'length' => 6,
            'alphabet' => 'tTrRiIfF1234567890',
        ],
    ],

];
