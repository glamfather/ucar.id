@extends('layouts.user-dashboard')
@section('htmlheader_title', 'Dashboard')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">

@endsection

@section('contentheader_title', '<i class="fa fa-dashboard"></i>&nbsp; Dashboard')

@section('breadcrumb')
    <li class="active"><i class="fa fa-dashboard"></i>&nbsp; Dahsboard</li>
@endsection


@section('main_content')
<div class="box">
	<div class="box-body">
		Halaman Home
	</div>
</div>

@endsection

@section('js_scripts')
@endsection