<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Homepage</title>
	<meta name="description" content="Halaman home">

	<link rel="apple-touch-icon" sizes="180x180" href="{{asset('frontendassets')}}/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="{{asset('frontendassets')}}/images/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="{{asset('frontendassets')}}/images/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="{{asset('frontendassets')}}/images/favicon/manifest.json">
	<link rel="mask-icon" href="{{asset('frontendassets')}}/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#999">

	<!-- Bootstrap -->
	<link href="{{asset('frontendassets')}}/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Date time picker -->
	<link href="{{asset('frontendassets')}}/plugins/datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">

	<!-- Scroll -->
	<link href="{{asset('frontendassets')}}/plugins/scroll/jquery.mCustomScrollbar.min.css" rel="stylesheet">

	<!-- Site -->
	<link href="{{asset('frontendassets')}}/plugins/webfont/font.css" rel="stylesheet">
	<link href="{{asset('frontendassets')}}/plugins/iconfont/iconfont.css" rel="stylesheet">
	<link href="{{asset('frontendassets')}}/css/main-site.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{asset('frontendassets')}}/js/html5shiv.min.js"></script>
	<script src="{{asset('frontendassets')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body class="homepage">
	<main class="container">
		<header id="home-head" class="home-head">
			<nav class="navbar navbar-default navbar-static-top">
				<div class="container-del">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="navbar-brand" href="index.html">
							<img class="img-responsive" src="{{asset('frontendassets')}}/images/logo.png" />
						</a>
					</div>

					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li><a href="#"><b class="bb-rounded">Cara Kerja</b></a></li>
							<li><a href="{{url('mobilku')}}"><b class="bb-rounded">Mobil Kamu</b></a></li>
							<li><a href="{{url('login')}}"><b class="bb-rounded">Masuk</b></a></li>
						</ul>
					</div>
				</div> <!-- //container -->
			</nav> <!-- //navbar -->
		</header>

		<section id="intro" class="intro" style="background-image:url('{{asset('frontendassets')}}/images/select-bg.jpg')">
			<div class="container-del position-relative">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-center">
						<h2 class="text-center text">Dapatkan Penghasilan 50 Juta Per Tahun <br /> Dengan Menyewakan Mobil Kamu</h2>
					</div> <!-- //col -->
				</div> <!-- //row -->

				<div class="tab-filter">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#mobil" class="no-radius" aria-controls="mobil" role="tab" data-toggle="tab"><i class="icon ic-directions-bus"></i> <b class="bb-rounded">Sewa Mobil</b></a>
						</li> <!-- //sewa -->

						<li role="presentation">
							<a href="#bandara" class="no-radius" aria-controls="bandara" role="tab" data-toggle="tab"><i class="icon ic-airplane-mode-on"></i> <b class="bb-rounded">Bandara</b></a>
						</li> <!-- //bandara -->

						<li role="presentation">
							<a href="#taxi" class="no-radius" aria-controls="taxi" role="tab" data-toggle="tab"><i class="icon ic-directions-car"></i> <b class="bb-rounded">Taksi</b></a>
						</li> <!-- //taxi -->
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="mobil">
							<form action="{{url('search')}}" target="_blank" method="get"3>
								<div class="row row-small-del">
									<div class="col-xs-12 col-sm-5">
										<div class="form-group inline-input-single">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon1"><i class="icon ic-location"></i></span>
												<input type="text" id="lj" class="form-control typeahead no-padding-left no-border-left" placeholder="Lokasi jemput" data-provide="typeahead" required="required" />
												<input type="hidden" name="t" id="t" value="n">
												<input type="hidden" name="ljLat" id="ljLat" value="">
												<input type="hidden" name="ljLng" id="ljLng" value="">
											</div>
										</div> <!-- //query -->
									</div> <!-- //col -->

									<div class="col-xs-12 col-sm-3 form-group">
										<div class="form-group">
											<div class="inline-input">
												<div class="col-xs-7 no-padding">
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
														<input type='text' class="form-control no-padding-left no-border-left" id='start-date' placeholder="Tanggal mulai" readonly="true" required="required" />
													</div>
												</div>

												<div class="col-xs-5 no-padding">
													<div class="input-group">
														<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
														<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='start-time' placeholder="Jam" readonly="true" required="required" />
													</div>
												</div>
											</div> <!-- //inline input -->
										</div> <!-- //select -->
									</div> <!-- //col -->

									<div class="col-xs-12 col-sm-3 form-group">
										<div class="form-group">
											<div class="inline-input">
												<div class="col-xs-7 no-padding">
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
														<input type='text' class="form-control no-padding-left no-border-left" id='end-date' placeholder="Tanggal selesai" readonly="true" />
													</div>
												</div>

												<div class="col-xs-5 no-padding">
													<div class="input-group">
														<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
														<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='end-time' placeholder="Jam" readonly="true" />
													</div>
												</div>
											</div> <!-- //inline input -->
										</div> <!-- //select -->
									</div> <!-- //col -->

									<div class="col-xs-12 col-sm-1">
										<div class="form-group">
											<button type="submit" class="btn btn-block btn-primary"><b>Cari</b></button>
										</div> <!-- //select -->
									</div> <!-- //col -->
								</div> <!-- //row -->
							</form>
						</div> <!-- //sewa -->

						<div role="tabpanel" class="tab-pane" id="bandara">
							<div class="tab-airport">
								<!-- Nav tabs -->
								<ul class="nav nav-pills" role="tablist">
									<li role="presentation" class="active"><a href="#a-to-b" aria-controls="a-to-b" role="tab" data-toggle="tab">Lokasi Jemput ke Bandara</a></li>

									<li role="presentation"><a href="#b-to-a" aria-controls="b-to-a" role="tab" data-toggle="tab">Bandara ke Lokasi Tujuan</a></li>
								</ul>

								<!-- Tab panes -->
								<div class="tab-content no-padding-left no-padding-bottom no-padding-right no-border">
									<div role="tabpanel" class="tab-pane active" id="a-to-b">
										<form action="{{url('search')}}" target="_blank" method="get">
											<div class="row row-small-del">
												<div class="col-xs-12 col-sm-4">
													<div class="form-group inline-input-single">
														<div class="input-group">
															<span class="input-group-addon" id="basic-addon1"><i class="icon ic-location"></i></span>
															<input type="text" id="from-airport" class="form-control typeahead no-padding-left no-border-left" placeholder="Lokasi jemput" data-provide="typeahead" />
														</div>
													</div> <!-- //query -->
												</div> <!-- //col -->

												<div class="col-xs-12 col-sm-3 form-group">
													<div class="form-group">
														<div class="inline-input">
															<div class="col-xs-7 no-padding">
																<div class="input-group">
																	<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
																	<input type='text' class="form-control no-padding-left no-border-left" id='start-date-airport' placeholder="Tanggal mulai" readonly="true" />
																</div>
															</div>

															<div class="col-xs-5 no-padding">
																<div class="input-group">
																	<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
																	<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='start-time-airport' placeholder="Jam" readonly="true" />
																</div>
															</div>
														</div> <!-- //inline input -->
													</div> <!-- //select -->
												</div> <!-- //col -->

												<div class="col-xs-12 col-sm-4">
													<div class="form-group inline-input-single">
														<div class="input-group">
															<span class="input-group-addon" id="basic-addon1"><i class="icon ic-airplane-mode-on"></i></span>
															<input type="text" id="airport" class="form-control typeahead airport no-padding-left no-border-left" placeholder="Bandara tujuan" data-provide="typeahead" />
														</div>
													</div> <!-- //query -->
												</div> <!-- //col -->

												<div class="col-xs-12 col-sm-1">
													<div class="form-group">
														<button type="submit" class="btn btn-block btn-primary"><b>Cari</b></button>
													</div> <!-- //select -->
												</div> <!-- //col -->
											</div> <!-- //row -->
										</form>
									</div> <!-- //a-to-b -->

									<div role="tabpanel" class="tab-pane no-padding no-border" id="b-to-a">
										<form action="listing.html">
											<div class="row row-small-del">
												<div class="col-xs-12 col-sm-4">
													<div class="form-group inline-input-single">
														<div class="input-group">
															<span class="input-group-addon" id="basic-addon1"><i class="icon ic-airplane-mode-on"></i></span>
															<input type="text" id="airport-b" class="form-control typeahead airport no-padding-left no-border-left" placeholder="Bandara tujuan" data-provide="typeahead" />
														</div>
													</div> <!-- //query -->
												</div> <!-- //col -->

												<div class="col-xs-12 col-sm-3 form-group">
													<div class="form-group">
														<div class="inline-input">
															<div class="col-xs-7 no-padding">
																<div class="input-group">
																	<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
																	<input type='text' class="form-control no-padding-left no-border-left" id='start-date-airport' placeholder="Tanggal mulai" readonly="true" />
																</div>
															</div>

															<div class="col-xs-5 no-padding">
																<div class="input-group">
																	<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
																	<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='start-time-airport' placeholder="Jam" readonly="true" />
																</div>
															</div>
														</div> <!-- //inline input -->
													</div> <!-- //select -->
												</div> <!-- //col -->

												<div class="col-xs-12 col-sm-4">
													<div class="form-group inline-input-single">
														<div class="input-group">
															<span class="input-group-addon" id="basic-addon1"><i class="icon ic-location"></i></span>
															<input type="text" id="from-airport-b" class="form-control typeahead no-padding-left no-border-left" placeholder="Lokasi jemput" data-provide="typeahead" />
														</div>
													</div> <!-- //query -->
												</div> <!-- //col -->

												<div class="col-xs-12 col-sm-1">
													<div class="form-group">
														<button type="submit" class="btn btn-block btn-primary"><b>Cari</b></button>
													</div> <!-- //select -->
												</div> <!-- //col -->
											</div> <!-- //row -->
										</form>
									</div> <!-- //b-to-a -->
								</div>

							</div>
						</div> <!-- //bandara -->

						<div role="tabpanel" class="tab-pane" id="taxi">
							<form action="listing.html">
								<div class="row row-small-del">
									<div class="col-xs-12 col-sm-5">
										<div class="form-group inline-input-single">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon1"><i class="icon ic-location"></i></span>
												<input type="text" id="from-taxi" class="form-control typeahead no-padding-left no-border-left" placeholder="Lokasi jemput" data-provide="typeahead" />
											</div>
										</div> <!-- //query -->
									</div> <!-- //col -->

									<div class="col-xs-12 col-sm-3 form-group">
										<div class="form-group">
											<div class="inline-input">
												<div class="col-xs-7 no-padding">
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
														<input type='text' class="form-control no-padding-left no-border-left" id='start-date-taxi' placeholder="Tanggal mulai" readonly="true" />
													</div>
												</div>

												<div class="col-xs-5 no-padding">
													<div class="input-group">
														<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
														<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='start-time-taxi' placeholder="Jam" readonly="true" />
													</div>
												</div>
											</div> <!-- //inline input -->
										</div> <!-- //select -->
									</div> <!-- //col -->

									<div class="col-xs-12 col-sm-3 form-group">
										<div class="form-group">
											<div class="inline-input">
												<div class="col-xs-7 no-padding">
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
														<input type='text' class="form-control no-padding-left no-border-left" id='end-date-taxi' placeholder="Tanggal selesai" readonly="true" />
													</div>
												</div>

												<div class="col-xs-5 no-padding">
													<div class="input-group">
														<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
														<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='end-time-taxi' placeholder="Jam" readonly="true" />
													</div>
												</div>
											</div> <!-- //inline input -->
										</div> <!-- //select -->
									</div> <!-- //col -->

									<div class="col-xs-12 col-sm-1">
										<div class="form-group">
											<button type="submit" class="btn btn-block btn-primary"><b>Cari</b></button>
										</div> <!-- //select -->
									</div> <!-- //col -->
								</div> <!-- //row -->
							</form>
						</div> <!-- //taxi -->
					</div> <!-- //tab -->
				</div> <!-- //tab container -->
			</div> <!-- //container -->
		</section> <!-- //intro -->

		<section id="featured" class="featured">
			<div class="container-del">
				<div class="row row-small">
					<div class="col-xs-6 col-sm-3">
						<a href="#" class="block box-wrap">
							<div class="box same-height block" style="background-image:url('{{asset('frontendassets')}}/images/bn1.jpg')">
							</div> <!-- //box -->

							<div class="f-name">
								<div class="f-title"><b class="bb-rounded">Ucar Rental</b></div>
								<div class="f-subtitle">Harga Sewa Mobil</div>
							</div>
						</a>
					</div> <!-- //col -->

					<div class="col-xs-6 col-sm-3">
						<a href="#" class="block box-wrap">
							<div class="box same-height block" style="background-image:url('{{asset('frontendassets')}}/images/bn2.jpg')">
							</div> <!-- //box -->

							<div class="f-name">
								<div class="f-title"><b class="bb-rounded">Ucar Rental</b></div>
								<div class="f-subtitle">Harga Sewa Mobil</div>
							</div>
						</a>
					</div> <!-- //col -->

					<div class="col-xs-6 col-sm-3">
						<a href="#" class="block box-wrap">
							<div class="box same-height block" style="background-image:url('{{asset('frontendassets')}}/images/bn3.jpg')">
							</div> <!-- //box -->

							<div class="f-name">
								<div class="f-title"><b class="bb-rounded">Ucar Rental</b></div>
								<div class="f-subtitle">Harga Sewa Mobil</div>
							</div>
						</a>
					</div> <!-- //col -->

					<div class="col-xs-6 col-sm-3">
						<a href="#" class="block box-wrap">
							<div class="box same-height block" style="background-image:url('{{asset('frontendassets')}}/images/bn1.jpg')">
							</div> <!-- //box -->

							<div class="f-name">
								<div class="f-title"><b class="bb-rounded">Ucar Rental</b></div>
								<div class="f-subtitle">Harga Sewa Mobil</div>
							</div>
						</a>

					</div> <!-- //col -->
				</div> <!-- //row -->
			</div> <!-- //container -->
		</section> <!-- //featured -->

		<section id="quickchoice" class="quickchoice">
		
			<div class="container-del">
				<div class="text-title text-uppercase text-white">
					<b class="bb-rounded">Tujuan Terbaik</b>
				</div> <!-- //title -->

				<div class="tabs">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs tab-destinations" role="tablist">
						<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Jakarta</a></li>
						<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Bandung</a></li>
						<li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Yogyakarta</a></li>
						<li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Palembang</a></li>
						<li role="presentation"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">Medan</a></li>
						<li role="presentation"><a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">Aceh</a></li>
						<li role="presentation"><a href="#tab7" aria-controls="tab7" role="tab" data-toggle="tab">Jambi</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content tab-content-destinations">
						<div role="tabpanel" class="tab-pane active" id="tab1">
							<div class="row">
								<div class="col-xs-12 col-sm-6 no-padding">
									<div class="q-slide">
										<div id="sliders" class="carousel slide" data-ride="carousel">
											<!-- Wrapper for slides -->
											<div class="carousel-inner" role="listbox">
												<div class="item active" style="background-image:url('{{asset('frontendassets')}}/images/sliders-home/sliders-1.jpg')"></div> <!-- //item slide -->

												<div class="item" style="background-image:url('{{asset('frontendassets')}}/images/sliders-home/sliders-1.jpg')"></div> <!-- //item slide -->
											</div>

											<!-- Controls -->
											<a class="left carousel-control" href="#sliders" role="button" data-slide="prev">
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											</a>

											<a class="right carousel-control" href="#sliders" role="button" data-slide="next">
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											</a>
										</div>
									</div> <!-- //slide wrap -->
								</div> <!-- //col -->

								<div class="col-xs-12 col-sm-6 no-padding">
									<div class="list-scroll">
										<div class="tab-place">
											<!-- Nav tabs -->
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation" class="active"><a href="#p-sewa" aria-controls="p-sewa" role="tab" data-toggle="tab"><b class="bb-rounded text-uppercase"><i class="icon ic-directions-bus"></i> Sewa Mobil</b></a></li>

												<li role="presentation"><a href="#p-bandara" id="p-bdr" aria-controls="p-bandara" role="tab" data-toggle="tab"><b class="bb-rounded text-uppercase"><i class="icon ic-airplane-mode-on"></i> Bandara</b></a></li>
											</ul>

											<!-- Tab panes -->
											<div class="tab-content">
												<div role="tabpanel" class="tab-pane active" id="p-sewa">
													<ul id="the-scroll" class="media-list">
														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->
													</ul> <!-- //list book -->
												</div> <!-- //sewa -->

												<div role="tabpanel" class="tab-pane" id="p-bandara">
													<ul id="the-scroll" class="media-list">
														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->
													</ul> <!-- //list book -->
												</div> <!-- //bandara -->
											</div> <!-- /tab content -->
										</div> <!-- //tab place -->
									</div> <!-- //scroll -->
								</div> <!-- //col -->
							</div> <!-- //row -->
						</div> <!-- // Jakarta -->

						<div role="tabpanel" class="tab-pane" id="tab2">
							<div class="row">
								<div class="col-xs-12 col-sm-6 no-padding">
									<div class="q-slide">
										<div id="sliders" class="carousel slide" data-ride="carousel">
											<!-- Wrapper for slides -->
											<div class="carousel-inner" role="listbox">
												<div class="item active" style="background-image:url('{{asset('frontendassets')}}/images/sliders-home/bike.jpg')"></div> <!-- //item slide -->

												<div class="item" style="background-image:url('{{asset('frontendassets')}}/images/sliders-home/bike.jpg')"></div> <!-- //item slide -->
											</div>

											<!-- Controls -->
											<a class="left carousel-control" href="#sliders" role="button" data-slide="prev">
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											</a>

											<a class="right carousel-control" href="#sliders" role="button" data-slide="next">
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											</a>
										</div>
									</div> <!-- //slide wrap -->
								</div> <!-- //col -->

								<div class="col-xs-12 col-sm-6 no-padding">
									<div class="list-scroll">
										<div class="tab-place">
											<!-- Nav tabs -->
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation" class="active"><a href="#p-sewa-1" aria-controls="p-sewa-1" role="tab" data-toggle="tab"><b class="bb-rounded text-uppercase"><i class="icon ic-directions-bus"></i> Sewa Mobil</b></a></li>

												<li role="presentation"><a href="#p-bandara-1" id="p-bdr" aria-controls="p-bandara-1" role="tab" data-toggle="tab"><b class="bb-rounded text-uppercase"><i class="icon ic-airplane-mode-on"></i> Bandara</b></a></li>
											</ul>

											<!-- Tab panes -->
											<div class="tab-content">
												<div role="tabpanel" class="tab-pane active" id="p-sewa-1">
													<ul id="the-scroll" class="media-list">
														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->
													</ul> <!-- //list book -->
												</div> <!-- //sewa -->

												<div role="tabpanel" class="tab-pane" id="p-bandara-1">
													<ul id="the-scroll" class="media-list">
														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->

														<li class="media">
															<div class="media-body">
																<div class="row">
																	<div class="col-xs-12 col-sm-12 col-md-5 no-padding-right">
																		<div class="l-name text-uppercase"><b class="bb-rounded">Toyota Avanza</b></div> <!-- //name -->

																		<div class="l-detail text-uppercase text-muted"><small>Tahun 2016 - termasuk bensin dan sopir</small></div>
																	</div> <!-- //col -->

																	<div class="col-xs-8 col-sm-8 col-md-4">
																		<div class="l-price position-relative">
																			<div class="p-text position-absolute text-muted text-uppercase"><small class="block">Mulai</small> <small class="block">Dari</small></div>
																			<div class="p-nom text-right">
																				<b class="bb-rounded">RP 150.000</b></div>
																		</div> <!-- //price -->
																	</div> <!-- //col -->

																	<div class="col-xs-4 col-sm-4 col-md-3 no-padding-left">
																		<a href="#book" class="btn btn-default btn-block text-uppercase">Pesan Sekarang</a>
																	</div> <!-- //col -->
																</div> <!-- //row -->
															</div> <!-- //item body -->
														</li> <!-- //item -->
													</ul> <!-- //list book -->
												</div> <!-- //bandara -->
											</div> <!-- /tab content -->
										</div> <!-- //tab place -->
									</div> <!-- //scroll -->
								</div> <!-- //col -->
							</div> <!-- //row -->
						</div> <!-- // bandung -->
					</div> <!-- //tab content wrap -->
				</div> <!-- //tabs -->
			</div> <!-- //container -->
		</section> <!-- //quickchoice -->

		<section id="more-info" class="more-info">
			<div class="title-line text-center position-relative"><span><b class="bb-rounded text-uppercase">Dapatkan Penghasilan lebih</b></span></div> <!-- //title line -->

			<div class="m-images position-relative">
				<div style="background-image:url('{{asset('frontendassets')}}/images/m-bg.jpg')">
					<div class="m-content">
						<div class="m-inner">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6">
								</div> <!-- //col -->

								<div class="col-xs-12 col-sm-12 col-md-6">
									<div class="text-white m-title">Punya Mobil? Sewakan di Ucar.id</div>
									<div class="text-white m-title">Dapatkan penghasilan Min 50 Juta / Tahun</div>
									<a href="register.html" class="btn btn-default text-white text-uppercase">Sewakan Mobil Kamu</a>
								</div> <!-- //col -->
							</div> <!-- //row -->
						</div> <!-- //inner -->
					</div> <!-- //wrap content -->
				</div> <!-- //the image -->
			</div> <!-- //image -->
		</section> <!-- //more info -->

		<footer class="footer">
			<section id="menu-footer" class="menu-footer footer-menu">
				<div class="f-menu-title text-white text-uppercase"><b>Sewa mobil di Indonesia</b></div>

				<div class="container-dell">
					<div class="row row-small">
						<div class="col-xs-6 col-sm-3">
							<div class="menu">
								<ul class="media-list">
									<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
									<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
									<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
									<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
									<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
								</ul> <!-- //menu item -->
							</div>
						</div> <!-- //col -->

						<div class="col-xs-6 col-sm-3">
							<div class="menu">
								<ul class="media-list">
									<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
									<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
									<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
									<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
									<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
								</ul> <!-- //menu item -->
							</div>
						</div> <!-- //col -->

						<div class="col-xs-6 col-sm-3">
							<div class="bottom-destinations-xs"></div>

							<div class="menu">
								<ul class="media-list">
									<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
									<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
									<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
									<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
									<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
								</ul> <!-- //menu item -->
							</div>
						</div> <!-- //col -->

						<div class="col-xs-6 col-sm-3">
							<div class="bottom-destinations-xs"></div>

							<div class="menu">
								<ul class="media-list">
									<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
									<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
									<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
									<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
									<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
								</ul> <!-- //menu item -->
							</div>
						</div> <!-- //col -->
					</div> <!-- //row -->
				</div> <!-- //container -->
			</section> <!-- //menu footer -->

			<section id="menu-footer" class="menu-footer footer-menu">
				<div class="f-menu-title text-white text-uppercase"><b>Sewa mobil bandara di Indonesia</b></div>

				<div class="container-dell">
					<div class="row row-small">
						<div class="col-xs-6 col-sm-3">
							<div class="menu">
								<ul class="media-list">
									<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
									<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
									<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
									<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
									<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
								</ul> <!-- //menu item -->
							</div>
						</div> <!-- //col -->

						<div class="col-xs-6 col-sm-3">
							<div class="menu">
								<ul class="media-list">
									<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
									<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
									<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
									<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
									<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
								</ul> <!-- //menu item -->
							</div>
						</div> <!-- //col -->

						<div class="col-xs-6 col-sm-3">
							<div class="bottom-destinations-xs"></div>

							<div class="menu">
								<ul class="media-list">
									<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
									<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
									<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
									<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
									<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
								</ul> <!-- //menu item -->
							</div>
						</div> <!-- //col -->

						<div class="col-xs-6 col-sm-3">
							<div class="bottom-destinations-xs"></div>

							<div class="menu">
								<ul class="media-list">
									<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
									<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
									<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
									<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
									<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
								</ul> <!-- //menu item -->
							</div>
						</div> <!-- //col -->
					</div> <!-- //row -->
				</div> <!-- //container -->
			</section> <!-- //menu footer -->

			<section id="other" class="other">
				<div class="row row-small">
					<div class="col-xs-12 col-sm-6">
						<div class="f-menu-title text-white text-uppercase"><b>Metode Pembayaran</b></div>

						<ul class="no-margin list-inline list-bank">
							<li><img src="{{asset('frontendassets')}}/images/bank/bri.png" /></li>
							<li><img src="{{asset('frontendassets')}}/images/bank/bni.png" /></li>
							<li><img src="{{asset('frontendassets')}}/images/bank/mandiri.png" /></li>
							<li><img src="{{asset('frontendassets')}}/images/bank/bca.png" /></li>
							<li><img src="{{asset('frontendassets')}}/images/bank/bri.png" /></li>
							<li><img src="{{asset('frontendassets')}}/images/bank/bni.png" /></li>
							<li><img src="{{asset('frontendassets')}}/images/bank/mandiri.png" /></li>
							<li><img src="{{asset('frontendassets')}}/images/bank/bca.png" /></li>
						</ul> <!-- //list bank -->
					</div> <!-- //col -->

					<div class="col-xs-12 col-sm-3">
						<div class="f-menu-title text-white text-uppercase"><b>Layanan Terverifikasi</b></div>

						<ul class="no-margin list-inline list-bank">
							<li><img src="{{asset('frontendassets')}}/images/bank/GeoTrust.png" /></li>
						</ul> <!-- //list bank -->
					</div> <!-- //col -->

					<div class="col-xs-12 col-sm-3">
						<div class="f-menu-title text-white text-uppercase"><b>Ikuti Ucar</b></div>

						<ul class="list-inline social-media no-margin-bottom">
							<li><a href="#fb" class="icon ic-post-facebook"></a></li>
							<li><a href="#tw" class="icon ic-post-twitter"></a></li>
							<li><a href="#g+" class="icon ic-post-gplus"></a></li>
							<li><a href="#bl" class="icon ic-post-blogger"></a></li>
						</ul> <!-- //social media -->
					</div> <!-- //col -->
				</div> <!-- //row -->
			</section> <!-- //menu footer -->

			<section id="about-home" class="about-home">
				<div class="row">
					<div class="col-xs-12 col-sm-7">
						<ul class="no-margin list-inline">
							<li><a href="#"><b class="bb">Mengenai Kami</b></a></li>
							<li><a href="#"><b class="bb">Hubungi Kami</b></a></li>
							<li><a href="faq.html"><b class="bb">FAQ</b></a></li>
							<li><a href="#"><b class="bb">Syarat &amp; Ketentuan</b></a></li>
							<li><a href="#"><b class="bb">Privacy Policy</b></a></li>
						</ul> <!-- //about -->
					</div> <!-- //col -->

					<div class="col-xs-12 col-sm-5 text-right">
						<b class="bb">© All Rights Reserved | PT. Ucar Global Network</b>
					</div> <!-- //col -->
				</div> <!-- //row -->
			</section> <!-- //about home -->

			<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
			<script src="{{asset('frontendassets')}}/js/jquery.min.js"></script>
			<!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="{{asset('frontendassets')}}/plugins/bootstrap/js/bootstrap.min.js"></script>
			<script src="{{asset('frontendassets')}}/plugins/range/js/ion.rangeSlider.js"></script>
			<!-- Date time picker -->
			<script src="{{asset('frontendassets')}}/plugins/datepicker/js/moment-with-locales.min.js"></script>
			<script src="{{asset('frontendassets')}}/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
			<!-- Typeahead autocomplete -->
			<script src="{{asset('frontendassets')}}/plugins/typeahead/bootstrap3-typeahead.min.js"></script>
			<!-- Scroll -->
			<script src="{{asset('frontendassets')}}/plugins/scroll/jquery.mCustomScrollbar.concat.min.js"></script>
			<!-- Site -->
			<script src="{{asset('frontendassets')}}/js/grid.js"></script>
			<script src="{{asset('frontendassets')}}/js/site.js"></script>
			
			{{-- Google Maps --}}
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuzwI2s2bf2lDglKewNMEvqeF_nqVvouE&libraries=places"></script>
			<script type="text/javascript">
			    function initialize() {
			        var input = document.getElementById('lj');
			        var autocomplete = new google.maps.places.Autocomplete(input);
			        google.maps.event.addListener(autocomplete, 'place_changed', function () {
			            var place = autocomplete.getPlace();
			            console.log(place);
			            $('#ljLat').val(place.geometry.location.lat());
			            $('#ljLng').val(place.geometry.location.lng());
			            // document.getElementById('city2').value = place.name;
			            // document.getElementById('ljLat').value = place.geometry.location.lat();
			            // document.getElementById('ljLng').value = place.geometry.location.lng();
			            //alert("This function is working!");
			            //alert(place.geometry.location.lat());
			           // alert(place.address_components[0].long_name);

			        });
			    }
			    google.maps.event.addDomListener(window, 'load', initialize); 
			</script>
			<script>
				$(document).ready(function() {
					$(window).keydown(function(event){
				    	if(event.keyCode == 13) {
				    		event.preventDefault();
				    		return false;
						}
					});
				});
				// var searchBox = new google.maps.places.Autocomplete(document.getElementById('lj'));
				// google.maps.event.addListener(searchBox, 'place_changed', function () {
	            	// place = searchBox.getPlace();
		            // document.getElementById('city2').value = place.name;
		            // document.getElementById('ljLat').value = place.geometry.location.lat();
		            // document.getElementById('ljLng').value = place.geometry.location.lng();
		            //alert("This function is working!");
		            //alert(place.name);
		           // alert(place.address_components[0].long_name);

		        // });
			</script>
		</footer>
	</main>
</body>
</html>
