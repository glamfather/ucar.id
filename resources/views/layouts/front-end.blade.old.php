<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Ucar.id</title>
	<meta name="description" content="Ucar.id Sewa Mobil">
	
	<link rel="apple-touch-icon" sizes="180x180" href="frontendassets/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="frontendassets/images/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="frontendassets/images/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="frontendassets/images/favicon/manifest.json">
	<link rel="mask-icon" href="frontendassets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">

	<!-- Bootstrap -->
	<link href="frontendassets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Date time picker -->
	<link href="frontendassets/plugins/datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">
	
	<!-- Site -->
	<link href="frontendassets/plugins/webfont/font.css" rel="stylesheet">
	<link href="frontendassets/plugins/iconfont/iconfont.css" rel="stylesheet">
	<link href="frontendassets/css/main-site.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/easyAutoSuggest.css">
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="frontendassets/js/html5shiv.min.js"></script>
	<script src="frontendassets/js/respond.min.js"></script>
	<![endif]-->
</head>
	
<body>
	<header id="home-head" class="home-head">
		<nav class="navbar navbar-default navbar-static-top position-absolute">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
          
					<a class="navbar-brand" href="index.html">
						<img class="img-responsive" src="frontendassets/images/logo-green.png" />
					</a>
				</div>
				
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a href="#">Cara Kerja</a></li>
						{{-- <li><a href="{{url('/mobilku')}}">Mobilku</a></li> --}}
						@if (Auth::guest())
							<li><a href="{{url('login')}}">Masuk</a></li>
							<li><a href="{{url('register')}}">Daftar</a></li>
						@else
							<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
							<li>
								<form method="post" action="{{url('/logout')}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <button type="submit" style="color:#fff;padding-top: 15px;padding-bottom: 15px;background: transparent;border:none;">{{ trans('adminlte_lang::message.signout') }}</button>
                                </form>
							</li>
						@endif
					</ul>
				</div>
			</div> <!-- //container -->
		</nav> <!-- //navbar -->
	</header>
	
	<main>
		<section id="intro" class="intro">
			<div class="container position-relative">
				<div class="row">
					<div class="col-xs-12 col-sm-10 col-md-8 col-center">
						<h2 class="text-center text"><b>More Than 200.000 cars worldwide, <br /> With the best rates available</b></h2>
					</div> <!-- //col -->
				</div> <!-- //row -->
		
				<div class="tab-filter">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#mobil" class="no-radius" aria-controls="mobil" role="tab" data-toggle="tab"><i class="icon ic-directions-bus"></i> Sewa Mobil</a>
						</li> <!-- //sewa -->
						
						<li role="presentation">
							<a href="#bandara" class="no-radius" aria-controls="bandara" role="tab" data-toggle="tab"><i class="icon ic-airplane-mode-on"></i> Bandara</a>
						</li> <!-- //bandara -->
						
						<li role="presentation">
							<a href="#taxi" class="no-radius" aria-controls="taxi" role="tab" data-toggle="tab"><i class="icon ic-directions-car"></i> Taksi</a>
						</li> <!-- //taxi -->
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="mobil">
							<form action="search" method="get">
								<div class="row row-small-del">
									<div class="col-xs-12 col-sm-5">
										<div class="form-group inline-input-single">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon1"><i class="icon ic-location"></i></span>
												<input type="text" class="form-control typeahead no-padding-left no-border-left" placeholder="Lokasi asjd" id="lokasiJemput" data-provide="typeahead" />
											</div>
										</div> <!-- //query -->
									</div> <!-- //col -->
									
									<div class="col-xs-12 col-sm-3 form-group">
										<div class="form-group">
											<div class="inline-input">
												<div class="col-xs-7 no-padding">
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
														<input type='text' class="form-control no-padding-left no-border-left" id='start-date' placeholder="Tanggal mulai" />
													</div>
												</div>
												
												<div class="col-xs-5 no-padding">
													<div class="input-group">
														<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
														<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='start-time' placeholder="Jam" />
													</div>
												</div>
											</div> <!-- //inline input -->
										</div> <!-- //select -->
									</div> <!-- //col -->
									
									<div class="col-xs-12 col-sm-3 form-group">
										<div class="form-group">
											<div class="inline-input">
												<div class="col-xs-7 no-padding">
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
														<input type='text' class="form-control no-padding-left no-border-left" id='end-date' placeholder="Tanggal selesai" />
													</div>
												</div>
												
												<div class="col-xs-5 no-padding">
													<div class="input-group">
														<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
														<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='end-time' placeholder="Jam" />
													</div>
												</div>
											</div> <!-- //inline input -->
										</div> <!-- //select -->
									</div> <!-- //col -->
		
									<div class="col-xs-12 col-sm-1">
										<div class="form-group">
											<button type="submit" class="btn btn-block btn-primary"><b>Cari</b></button>
										</div> <!-- //select -->
									</div> <!-- //col -->
								</div> <!-- //row -->
							</form>
						</div> <!-- //sewa -->
						
						<div role="tabpanel" class="tab-pane" id="bandara">
							<div class="tab-airport">
								<!-- Nav tabs -->
								<ul class="nav nav-pills" role="tablist">
									<li role="presentation" class="active"><a href="#a-to-b" aria-controls="a-to-b" role="tab" data-toggle="tab">Lokasi Jemput ke Bandara</a></li>
									
									<li role="presentation"><a href="#b-to-a" aria-controls="b-to-a" role="tab" data-toggle="tab">Bandara ke Lokasi Tujuan</a></li>
								</ul>

								<!-- Tab panes -->
								<div class="tab-content no-padding-left no-padding-bottom no-padding-right no-border">
									<div role="tabpanel" class="tab-pane active" id="a-to-b">
										<form action="bandara">
											<div class="row row-small-del">
												<div class="col-xs-12 col-sm-4">
													<div class="form-group inline-input-single">
														<div class="input-group">
															<span class="input-group-addon" id="basic-addon1"><i class="icon ic-location"></i></span>
															<input type="text" id="from-airport" class="form-control typeahead no-padding-left no-border-left" placeholder="Lokasi jemput" data-provide="typeahead" />
														</div>
													</div> <!-- //query -->
												</div> <!-- //col -->
												
												<div class="col-xs-12 col-sm-3 form-group">
													<div class="form-group">
														<div class="inline-input">
															<div class="col-xs-7 no-padding">
																<div class="input-group">
																	<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
																	<input type='text' class="form-control no-padding-left no-border-left" id='start-date-airport' placeholder="Tanggal mulai" />
																</div>
															</div>
															
															<div class="col-xs-5 no-padding">
																<div class="input-group">
																	<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
																	<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='start-time-airport' placeholder="Jam" />
																</div>
															</div>
														</div> <!-- //inline input -->
													</div> <!-- //select -->
												</div> <!-- //col -->
												
												<div class="col-xs-12 col-sm-4">
													<div class="form-group inline-input-single">
														<div class="input-group">
															<span class="input-group-addon" id="basic-addon1"><i class="icon ic-airplane-mode-on"></i></span>
															<input type="text" id="airport" class="form-control typeahead airport no-padding-left no-border-left" placeholder="Bandara tujuan" data-provide="typeahead" />
														</div>
													</div> <!-- //query -->
												</div> <!-- //col -->
					
												<div class="col-xs-12 col-sm-1">
													<div class="form-group">
														<button type="submit" class="btn btn-block btn-primary"><b>Cari</b></button>
													</div> <!-- //select -->
												</div> <!-- //col -->
											</div> <!-- //row -->
										</form>
									</div> <!-- //a-to-b -->
									
									<div role="tabpanel" class="tab-pane no-padding no-border" id="b-to-a">
										<form action="bandara-a">
											<div class="row row-small-del">
												<div class="col-xs-12 col-sm-4">
													<div class="form-group inline-input-single">
														<div class="input-group">
															<span class="input-group-addon" id="basic-addon1"><i class="icon ic-airplane-mode-on"></i></span>
															<input type="text" id="airport-b" class="form-control typeahead airport no-padding-left no-border-left" placeholder="Bandara tujuan" data-provide="typeahead" />
														</div>
													</div> <!-- //query -->
												</div> <!-- //col -->
												
												<div class="col-xs-12 col-sm-3 form-group">
													<div class="form-group">
														<div class="inline-input">
															<div class="col-xs-7 no-padding">
																<div class="input-group">
																	<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
																	<input type='text' class="form-control no-padding-left no-border-left" id='start-date-airport' placeholder="Tanggal mulai" />
																</div>
															</div>
															
															<div class="col-xs-5 no-padding">
																<div class="input-group">
																	<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
																	<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='start-time-airport' placeholder="Jam" />
																</div>
															</div>
														</div> <!-- //inline input -->
													</div> <!-- //select -->
												</div> <!-- //col -->
												
												<div class="col-xs-12 col-sm-4">
													<div class="form-group inline-input-single">
														<div class="input-group">
															<span class="input-group-addon" id="basic-addon1"><i class="icon ic-location"></i></span>
															<input type="text" id="from-airport-b" class="form-control typeahead no-padding-left no-border-left" placeholder="Lokasi jemput" data-provide="typeahead" />
														</div>
													</div> <!-- //query -->
												</div> <!-- //col -->
					
												<div class="col-xs-12 col-sm-1">
													<div class="form-group">
														<button type="submit" class="btn btn-block btn-primary"><b>Cari</b></button>
													</div> <!-- //select -->
												</div> <!-- //col -->
											</div> <!-- //row -->
										</form>
									</div> <!-- //b-to-a -->
								</div>

							</div>
						</div> <!-- //bandara -->
						
						<div role="tabpanel" class="tab-pane" id="taxi">
							<form action="taxi">
								<div class="row row-small-del">
									<div class="col-xs-12 col-sm-5">
										<div class="form-group inline-input-single">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon1"><i class="icon ic-location"></i></span>
												<input type="text" id="from-taxi" class="form-control typeahead no-padding-left no-border-left" placeholder="Lokasi jemput" data-provide="typeahead" />
											</div>
										</div> <!-- //query -->
									</div> <!-- //col -->
									
									<div class="col-xs-12 col-sm-3 form-group">
										<div class="form-group">
											<div class="inline-input">
												<div class="col-xs-7 no-padding">
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
														<input type='text' class="form-control no-padding-left no-border-left" id='start-date-taxi' placeholder="Tanggal mulai" />
													</div>
												</div>
												
												<div class="col-xs-5 no-padding">
													<div class="input-group">
														<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
														<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='start-time-taxi' placeholder="Jam" />
													</div>
												</div>
											</div> <!-- //inline input -->
										</div> <!-- //select -->
									</div> <!-- //col -->
									
									<div class="col-xs-12 col-sm-3 form-group">
										<div class="form-group">
											<div class="inline-input">
												<div class="col-xs-7 no-padding">
													<div class="input-group">
														<span class="input-group-addon" id="basic-addon1"><i class="icon ic-today"></i></span>
														<input type='text' class="form-control no-padding-left no-border-left" id='end-date-taxi' placeholder="Tanggal selesai" />
													</div>
												</div>
												
												<div class="col-xs-5 no-padding">
													<div class="input-group">
														<span class="input-group-addon no-radius" id="basic-addon1"><i class="icon ic-access-alarms"></i></span>
														<input type='text' class="form-control no-border-left no-padding-left padding-top-10" id='end-time-taxi' placeholder="Jam" />
													</div>
												</div>
											</div> <!-- //inline input -->
										</div> <!-- //select -->
									</div> <!-- //col -->
		
									<div class="col-xs-12 col-sm-1">
										<div class="form-group">
											<button type="submit" class="btn btn-block btn-primary"><b>Cari</b></button>
										</div> <!-- //select -->
									</div> <!-- //col -->
								</div> <!-- //row -->
							</form>
						</div> <!-- //taxi -->
					</div> <!-- //tab -->
				</div> <!-- //tab container -->
			</div> <!-- //container -->
		</section> <!-- //intro -->
		
		<section id="highlight" class="highlight">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="media">
							<div class="media-left media-middle">
								<div class="icon ic-brightness-auto"></div> <!-- //icon -->
							</div> <!-- //media -left" -->
							
							<div class="media-body media-middle">
								<h4 class="media-heading">Best Price Guaranteed</h4>
								<p class="no-margin">Find the best vgfdgdfgfdgfdg option among 250.000 hotels</p>
							</div> <!-- //media -body -->
						</div> <!-- //media -->
					</div> <!-- //col -->
					
					<div class="col-xs-12 col-sm-4">
						<div class="media">
							<div class="media-left media-middle">
								<div class="icon ic-directions-car"></div> <!-- //icon -->
							</div> <!-- //media -left" -->
							
							<div class="media-body media-middle">
								<h4 class="media-heading">Best Price Guaranteed</h4>
								<p class="no-margin">Find the best vgfdgdfgfdgfdg option among 250.000 hotels</p>
							</div> <!-- //media -body -->
						</div> <!-- //media -->
					</div> <!-- //col -->
					
					<div class="col-xs-12 col-sm-4">
						<div class="media">
							<div class="media-left media-middle">
								<div class="icon ic-directions-bike"></div> <!-- //icon -->
							</div> <!-- //media -left" -->
							
							<div class="media-body media-middle">
								<h4 class="media-heading">Best Price Guaranteed</h4>
								<p class="no-margin">Find the best vgfdgdfgfdgfdg option among 250.000 hotels</p>
							</div> <!-- //media -body -->
						</div> <!-- //media -->
					</div> <!-- //col -->
				</div> <!-- //row -->
			</div> <!-- //container -->
		</section> <!-- //highlight -->
		
		<section id="featured" class="featured">
			<div class="container">
				<div class="text-title text-uppercase">
					<b>Popular Destinations</b>
				</div> <!-- //title -->
				
				<div class="row row-small">
					<div class="col-xs-6 col-sm-3">
						<a href="#" class="box same-height block" style="background-image:url('assets/images/bn1.jpg')">
						</a> <!-- //box -->
					</div> <!-- //col -->
					
					<div class="col-xs-6 col-sm-3">
						<a href="#" class="box same-height block" style="background-image:url('assets/images/bn2.jpg')">
						</a> <!-- //box -->
					</div> <!-- //col -->
					
					<div class="col-xs-6 col-sm-3">
						<a href="#" class="box same-height block" style="background-image:url('assets/images/bn3.jpg')">
						</a> <!-- //box -->
					</div> <!-- //col -->
					
					<div class="col-xs-6 col-sm-3">
						<a href="#" class="box same-height block" style="background-image:url('assets/images/bn1.jpg')">
						</a> <!-- //box -->
					</div> <!-- //col -->
				</div> <!-- //row -->
			</div> <!-- //container -->
		</section> <!-- //featured -->
		
		<section id="quickchoice" class="quickchoice">
			<div class="container">
				<div class="text-title text-uppercase">
					<b>Popular Destinations</b>
				</div> <!-- //title -->
				
				<div class="tabs">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Jakarta</a></li>
						<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Bandung</a></li>
						<li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Yogyakarta</a></li>
						<li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Palembang</a></li>
						<li role="presentation"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">Medan</a></li>
						<li role="presentation"><a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">Aceh</a></li>
						<li role="presentation"><a href="#tab7" aria-controls="tab7" role="tab" data-toggle="tab">Jambi</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="tab1">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Sewa Mobil</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Bandara</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Taksi Online</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-info icon ic-check-circle"></i><span>GoCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-success icon ic-check-circle"></i><span>Uber</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GrabCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-success icon ic-check-circle"></i><span>Uber</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GrabCar</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GoCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
							</div> <!-- //row -->
						</div> <!-- //tab content -->
						
						<div role="tabpanel" class="tab-pane" id="tab2">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Sewa Mobil</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Bandara</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Taksi Online</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-info icon ic-check-circle"></i><span>GoCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-success icon ic-check-circle"></i><span>Uber</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GrabCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-success icon ic-check-circle"></i><span>Uber</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GrabCar</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GoCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
							</div> <!-- //row -->
						</div> <!-- //tab content -->
						
						<div role="tabpanel" class="tab-pane" id="tab3">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Sewa Mobil</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Bandara</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Taksi Online</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-info icon ic-check-circle"></i><span>GoCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-success icon ic-check-circle"></i><span>Uber</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GrabCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-success icon ic-check-circle"></i><span>Uber</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GrabCar</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GoCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
							</div> <!-- //row -->
						</div> <!-- //tab content -->
						
						<div role="tabpanel" class="tab-pane" id="tab4">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Sewa Mobil</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Bandara</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Taksi Online</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-info icon ic-check-circle"></i><span>GoCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-success icon ic-check-circle"></i><span>Uber</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GrabCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-success icon ic-check-circle"></i><span>Uber</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GrabCar</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GoCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
							</div> <!-- //row -->
						</div> <!-- //tab content -->
						
						<div role="tabpanel" class="tab-pane" id="tab5">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Sewa Mobil</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Bandara</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="title-content"><b>Taksi Online</b></div> <!-- //title -->
									<div class="subtitle-content text-muted">Jakarta 534 mobil</div> <!-- //subtitle -->
									
									<ul class="media-list no-margin">
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-info icon ic-check-circle"></i><span>GoCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-success icon ic-check-circle"></i><span>Uber</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GrabCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
										
										<a class="media block" href="#">
											<div class="media-left">
												<div class="image">
													<img class="img-circle" src="frontendassets/images/people/profile_user.jpg"></img>
												</div>
											</div> <!-- //image -->
											
											<div class="media-body">
												<div class="row row-small">
													<div class="col-xs-6">
														<h4 class="media-heading no-margin">Avanza - 2016</h4>
														<ul class="list-inline">
															<li><i class="text-success icon ic-check-circle"></i><span>Uber</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GrabCar</span></li>
															
															<li><i class="text-success icon ic-check-circle"></i><span>GoCar</span></li>
														</ul>
													</div>
													
													<div class="col-xs-6 no-padding-left">
														<div class="price">
															<div class="p-from">Dari</div>
															<sup>Rp</sup>
															<span class="nom">240.000</span>
														</div> <!-- //price -->
													</div>
												</div> <!-- //row -->
												
												<div class="review text-right">
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star"></i>
													<i class="icon ic-star-half"></i>
													<i class="icon ic-star text-muted"></i>
													
													<span class="text-muted">13 Reviews</span>
												</div> <!-- //review -->
											</div> <!-- //text -->
										</a> <!-- //media -->
									</ul> <!-- //list -->
								</div> <!-- //col -->
							</div> <!-- //row -->
						</div> <!-- //tab content -->
					</div>
				</div> <!-- //tabs -->
			</div> <!-- //container -->
		</section> <!-- //quickchoice -->
	</main>
	
	<footer class="footer">
		<section id="connect" class="connect">
			<div class="container">
				<div class="row row-small">
					<div class="col-xs-12 col-sm-3 no-padding-right text-center-xs">
						<ul class="list-inline social-media no-margin-bottom">
							<li><a href="#fb" class="icon ic-post-facebook"></a></li>
							<li><a href="#tw" class="icon ic-post-twitter"></a></li>
							<li><a href="#g+" class="icon ic-post-gplus"></a></li>
							<li><a href="#bl" class="icon ic-post-blogger"></a></li>
						</ul> <!-- //social media -->
					</div> <!-- //col -->
					
					<div class="col-xs-12 col-sm-4 col-sm-push-5 no-padding-left text-right text-center-xs">
						<ul class="list-inline no-margin-bottom">
							<li class="no-padding"><a href="#play" class="img-responsive" target="_blank"><img src="frontendassets/images/PS.svg" /></a></li>
							<li class="no-padding"><a href="#store" class="img-responsive" target="_blank"><img src="frontendassets/images/GS.svg" /></a></li>
						</ul>
					</div>
					
					<div class="col-xs-12 col-sm-5 col-sm-pull-4 text-center">
						<ul class="list-inline about-top no-margin-bottom">
							<li><a href="#"><b>About Us</b></a></li>
							<li><a href="#"><b>Contact Us</b></a></li>
							<li><a href="#"><b>FAQ</b></a></li>
							<li><a href="#"><b>Site Map</b></a></li>
						</ul>
					</div> <!-- //col -->
				</div> <!-- //row -->
				
				<div class="bottom-destinations"></div>
				
			</div> <!-- //container -->
		</section> <!-- //connect -->
		
		<section id="bank" class="bank">
			<div class="container">
				<div class="row">
					<div class="col-xs-4 col-sm-2">
						<img class="img-responsive" src="frontendassets/images/bank/bca.png" />
					</div> <!-- //col -->
					
					<div class="col-xs-4 col-sm-2">
						<img class="img-responsive" src="frontendassets/images/bank/bca.png" />
					</div> <!-- //col -->
					
					<div class="col-xs-4 col-sm-2">
						<img class="img-responsive" src="frontendassets/images/bank/bca.png" />
					</div> <!-- //col -->
					
					<div class="col-xs-4 col-sm-2">
						<img class="img-responsive" src="frontendassets/images/bank/bca.png" />
					</div> <!-- //col -->
					
					<div class="col-xs-4 col-sm-2">
						<img class="img-responsive" src="frontendassets/images/bank/bca.png" />
					</div> <!-- //col -->
					
					<div class="col-xs-4 col-sm-2">
						<img class="img-responsive" src="frontendassets/images/bank/bca.png" />
					</div> <!-- //col -->
				</div> <!-- //row -->
			</div> <!-- //container -->
		</section> <!-- //bank -->
		
		<section id="menu-footer" class="menu-footer">
			<div class="container">
				<div class="row row-small">
					<div class="col-xs-6 col-sm-3">
						<div class="menu">
							<div class="title"><a href="#"><b>UK &amp; Ireland</b></a></div>
							
							<ul class="media-list">
								<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
								<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
								<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
								<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
								<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
							</ul> <!-- //menu item -->
						</div>
					</div> <!-- //col -->
					
					<div class="col-xs-6 col-sm-3">
						<div class="menu">
							<div class="title"><a href="#"><b>UK &amp; Ireland</b></a></div>
							
							<ul class="media-list">
								<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
								<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
								<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
								<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
								<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
							</ul> <!-- //menu item -->
						</div>
					</div> <!-- //col -->
					
					<div class="col-xs-6 col-sm-3">
						<div class="bottom-destinations-xs"></div>
						
						<div class="menu">
							<div class="title"><a href="#"><b>UK &amp; Ireland</b></a></div>
							
							<ul class="media-list">
								<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
								<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
								<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
								<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
								<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
							</ul> <!-- //menu item -->
						</div>
					</div> <!-- //col -->
					
					<div class="col-xs-6 col-sm-3">
						<div class="bottom-destinations-xs"></div>
						
						<div class="menu">
							<div class="title"><a href="#"><b>UK &amp; Ireland</b></a></div>
							
							<ul class="media-list">
								<li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
								<li class="media"><a href="#">Sewa Mobil di Bali</a></li>
								<li class="media"><a href="#">Sewa Mobil di Medan</a></li>
								<li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
								<li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
							</ul> <!-- //menu item -->
						</div>
					</div> <!-- //col -->
				</div> <!-- //row -->
			</div> <!-- //container -->
		</section> <!-- //menu footer -->
		
		<section id="about" class="about">
			<div class="container">
				<div class="media">
					<div class="media-left media-middle">
						<div class="logo">
							<img src="frontendassets/images/logo-green.png" class="img-responsive" />
						</div> <!-- //logo -->
					</div> <!-- //media left -->
					
					<div class="media-body media-middle">
						<ul class="list-inline no-margin-bottom">
							<li><a href="#" class="block">Tentang Kami</a></li>
							<li><a href="#" class="block">FAQ</a></li>
							<li><a href="#" class="block">Syarat &amp; Ketentuan</a></li>
							<li><a href="#" class="block">Kebijakan Privasi</a></li>
						</ul>
						
						<p class="no-margin text-muted">Blabla.com – All Right Reserved</p>
					</div> <!-- //media-body-->
				</div> <!-- //media -->
			</div> <!-- //container -->
		</section> <!-- //about -->
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="frontendassets/js/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="frontendassets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<!-- Date time picker -->
		<script src="frontendassets/plugins/datepicker/js/moment-with-locales.min.js"></script>
		<script src="frontendassets/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
		<!-- Typeahead autocomplete -->
		<script src="frontendassets/plugins/typeahead/bootstrap3-typeahead.min.js"></script>
		<!-- Site -->
		<script src="frontendassets/js/grid.js"></script>
		<script src="frontendassets/js/site.js"></script>

	</footer>

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuzwI2s2bf2lDglKewNMEvqeF_nqVvouE&libraries=places"></script>
<script>
	var searchBox = new google.maps.places.SearchBox(document.getElementById('lokasiJemput'));
</script>
{{-- Airport --}}
<script src="js/easyAutoSuggest.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var options = {
        url: function(phrase) {
            return '{{url('airport/search')}}' + '/' + phrase;
        },
        getValue: 'name'
        // template: {
        //     type: "links",
        //     fields: {
        //         link: "link"
        //     }
        // },
        // list: {
        //     onChooseEvent: function () {
        //         var value = $('#form-airport').getSelectedItemData().link;
        //         window.open(value,'_blank');
        //     }
        // }
    };
    $('#from-airport').easyAutocomplete(options);
});

</script>
</body>
</html>