<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Registrasi</title>
    <meta name="description" content="Halaman home">

    <link rel="apple-touch-icon" sizes="180x180" href="frontendassets/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="frontendassets/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="frontendassets/images/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="frontendassets/images/favicon/manifest.json">
    <link rel="mask-icon" href="frontendassets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#999">

    <!-- Bootstrap -->
    <link href="frontendassets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Date time picker -->
    <link href="frontendassets/plugins/datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <!-- Site -->
    <link href="frontendassets/plugins/webfont/font.css" rel="stylesheet">
    <link href="frontendassets/plugins/iconfont/iconfont.css" rel="stylesheet">
    <link href="frontendassets/css/bootstrap-social.css" rel="stylesheet">
    <link href="frontendassets/css/main-site.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="frontendassets/js/html5shiv.min.js"></script>
    <script src="frontendassets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<header id="home-head" class="home-head">
    <nav class="navbar navbar-default navbar-static-top position-absolute">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="index.html">
                    <img class="img-responsive" src="frontendassets/images/logo-green.png" />
                </a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#">Cara Kerja</a></li>
                    <li><a href="#">Mobil Kamu</a></li>
                    <li><a href="#">Masuk</a></li>
                    <li><a href="#">Registrasi</a></li>
                </ul>
            </div>
        </div> <!-- //container -->
    </nav> <!-- //navbar -->
</header>

<main>
    <section id="intro" class="intro intro-login">
        <div class="container position-relative">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-8 col-center">
                    <h2 class="text-center text">Your next great trip is waiting. </h2>
                </div> <!-- //col -->
            </div> <!-- //row -->

            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 col-center">
                    <div class="login-area">
                        <div class="login-header">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="text-center"><a href="/login">Masuk</a></li>
                                <li role="presentation" class="active text-center"><a href="/register">Registrasi</a></li>
                                <li role="presentation" class="text-center"><a href="/lost-password">Lupa Password</a></li>
                            </ul>
                        </div> <!-- //head -->

                        <div class="title text-center">
                            <b>Sign up</b>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div> <!-- //title -->

                        <div class="with">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <a class="btn btn-block btn-lg btn-social btn-facebook">
                                        <span class="ic-post-facebook"></span> Connect with Facebook
                                    </a>
                                </div> <!-- //col -->

                                <div class="col-xs-12 col-sm-6">
                                    <a class="btn btn-block btn-lg btn-social btn-google">
                                        <span class="ic-post-gplus"></span> Connect with Google
                                    </a>
                                </div> <!-- //col -->
                            </div> <!-- //row -->
                        </div> <!-- //login with -->

                        <div class="form">
                            <form method="post" action="{{url('register')}}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control no-shadow" placeholder="Full Name" required/>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control no-shadow" placeholder="Email" required/>
                                </div>

                                <div class="form-group position-relative">
                                    <input type="password" name="password" id="form-register-password"  class="form-control" placeholder="Password" required>

                                    <ul id="password-strength-meter" class="row">
                                        <li class="col-xs-3"></li>
                                        <li class="col-xs-3"></li>
                                        <li class="col-xs-3"></li>
                                        <li class="col-xs-3"></li>
                                    </ul>
                                </div>

                                <div class="form-group position-relative">
                                    <input type="password" name="password_confirmation" id="form-register-repassword" class="form-control" placeholder="Repeat Password" value="" required>
                                    <div class="password-match"></div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-8">
                                        <p>By creating an account you agree to our <a href="http://www.idaff.com/tos" target="blank">Terms and Conditions</a> and <a href="http://www.idaff.com/privacypolicy">Privacy Policy</a></p>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <button id="form-register-submit" type="submit" class="btn btn-danger btn-block" name="register" value="register">Sign Up Now</button>
                                    </div>
                                </div>
                            </form> <!-- //the form -->
                        </div> <!-- //form -->
                    </div> <!-- //login area -->
                </div> <!-- //col -->
            </div> <!-- //row -->
        </div> <!-- //container -->
    </section> <!-- //intro -->
</main>

<footer class="footer">
    <section id="connect" class="connect">
        <div class="container">
            <div class="row row-small">
                <div class="col-xs-12 col-sm-3 no-padding-right text-center-xs">
                    <ul class="list-inline social-media no-margin-bottom">
                        <li><a href="#fb" class="icon ic-post-facebook"></a></li>
                        <li><a href="#tw" class="icon ic-post-twitter"></a></li>
                        <li><a href="#g+" class="icon ic-post-gplus"></a></li>
                        <li><a href="#bl" class="icon ic-post-blogger"></a></li>
                    </ul> <!-- //social media -->
                </div> <!-- //col -->

                <div class="col-xs-12 col-sm-4 col-sm-push-5 no-padding-left text-right text-center-xs">
                    <ul class="list-inline no-margin-bottom">
                        <li class="no-padding"><a href="#play" class="img-responsive" target="_blank"><img src="frontendassets/images/PS.svg" /></a></li>
                        <li class="no-padding"><a href="#store" class="img-responsive" target="_blank"><img src="frontendassets/images/GS.svg" /></a></li>
                    </ul>
                </div>

                <div class="col-xs-12 col-sm-5 col-sm-pull-4 text-center">
                    <ul class="list-inline about-top no-margin-bottom">
                        <li><a href="#"><b>About Us</b></a></li>
                        <li><a href="#"><b>Contact Us</b></a></li>
                        <li><a href="#"><b>FAQ</b></a></li>
                        <li><a href="#"><b>Site Map</b></a></li>
                        </li>
                </div> <!-- //col -->
            </div> <!-- //row -->

            <div class="bottom-destinations"></div>

        </div> <!-- //container -->
    </section> <!-- //connect -->

    <section id="bank" class="bank">
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-2">
                    <img class="img-responsive" src="frontendassets/images/bank/bca.png" />
                </div> <!-- //col -->

                <div class="col-xs-4 col-sm-2">
                    <img class="img-responsive" src="frontendassets/images/bank/bca.png" />
                </div> <!-- //col -->

                <div class="col-xs-4 col-sm-2">
                    <img class="img-responsive" src="frontendassets/images/bank/bca.png" />
                </div> <!-- //col -->

                <div class="col-xs-4 col-sm-2">
                    <img class="img-responsive" src="frontendassets/images/bank/bca.png" />
                </div> <!-- //col -->

                <div class="col-xs-4 col-sm-2">
                    <img class="img-responsive" src="frontendassets/images/bank/bca.png" />
                </div> <!-- //col -->

                <div class="col-xs-4 col-sm-2">
                    <img class="img-responsive" src="frontendassets/images/bank/bca.png" />
                </div> <!-- //col -->
            </div> <!-- //row -->
        </div> <!-- //container -->
    </section> <!-- //bank -->

    <section id="menu-footer" class="menu-footer">
        <div class="container">
            <div class="row row-small">
                <div class="col-xs-6 col-sm-3">
                    <div class="menu">
                        <div class="title"><a href="#"><b>UK &amp; Ireland</b></a></div>

                        <ul class="media-list">
                            <li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Bali</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Medan</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
                        </ul> <!-- //menu item -->
                    </div>
                </div> <!-- //col -->

                <div class="col-xs-6 col-sm-3">
                    <div class="menu">
                        <div class="title"><a href="#"><b>UK &amp; Ireland</b></a></div>

                        <ul class="media-list">
                            <li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Bali</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Medan</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
                        </ul> <!-- //menu item -->
                    </div>
                </div> <!-- //col -->

                <div class="col-xs-6 col-sm-3">
                    <div class="bottom-destinations-xs"></div>

                    <div class="menu">
                        <div class="title"><a href="#"><b>UK &amp; Ireland</b></a></div>

                        <ul class="media-list">
                            <li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Bali</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Medan</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
                        </ul> <!-- //menu item -->
                    </div>
                </div> <!-- //col -->

                <div class="col-xs-6 col-sm-3">
                    <div class="bottom-destinations-xs"></div>

                    <div class="menu">
                        <div class="title"><a href="#"><b>UK &amp; Ireland</b></a></div>

                        <ul class="media-list">
                            <li class="media"><a href="#">Sewa Mobil di Bandung</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Bali</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Medan</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Yogyakarta</a></li>
                            <li class="media"><a href="#">Sewa Mobil di Jakarta</a></li>
                        </ul> <!-- //menu item -->
                    </div>
                </div> <!-- //col -->
            </div> <!-- //row -->
        </div> <!-- //container -->
    </section> <!-- //menu footer -->

    <section id="about" class="about">
        <div class="container">
            <div class="media">
                <div class="media-left media-middle">
                    <div class="logo">
                        <img src="frontendassets/images/logo-green.png" class="img-responsive" />
                    </div> <!-- //logo -->
                </div> <!-- //media left -->

                <div class="media-body media-middle">
                    <ul class="list-inline no-margin-bottom">
                        <li><a href="#" class="block">Tentang Kami</a></li>
                        <li><a href="#" class="block">FAQ</a></li>
                        <li><a href="#" class="block">Syarat &amp; Ketentuan</a></li>
                        <li><a href="#" class="block">Kebijakan Privasi</a></li>
                    </ul>

                    <p class="no-margin text-muted">Blabla.com – All Right Reserved</p>
                </div> <!-- //media-body-->
            </div> <!-- //media -->
        </div> <!-- //container -->
    </section> <!-- //about -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="frontendassets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="frontendassets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- Date time picker -->
    <script src="frontendassets/plugins/datepicker/js/moment-with-locales.min.js"></script>
    <script src="frontendassets/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
    <!-- Typeahead autocomplete -->
    <script src="frontendassets/plugins/typeahead/bootstrap3-typeahead.min.js"></script>
    <!-- Password streght indicator -->
    <script src="frontendassets/js/zxcvbn.min.js"></script>
    <script src="frontendassets/js/regpass.js"></script>
    <!-- Site -->
    <script src="frontendassets/js/grid.js"></script>
    <script src="frontendassets/js/site.js"></script>
</footer>
</body>
</html>