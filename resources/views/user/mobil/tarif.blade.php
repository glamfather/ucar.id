@extends('layouts.user-dashboard')
@section('htmlheader_title', 'Tarif Mobilku')

@section('css')

@endsection

@section('contentheader_title', '<i class="fa fa-dollar"></i>&nbsp; Tarif')

@section('breadcrumb')
    <li class=""><a href="/dashboard"><i class="fa fa-dashboard"></i>&nbsp; Dahsboard</a></li>
    <li><a href="/mobilku"> <i class="fa fa-car"></i>&nbsp; Mobilku</a></li>
    <li class="active">Tarif</li>
@endsection


@section('main_content')
    <div class="box">
        <div class="box-header">
        </div>
        <div class="box-body">
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif

            <div>
                <table class="table table-bordered table-condensed table-striped">
                    <thead>
                    </thead>
                    <tr>
                        <th>No</th>
                        <th>Mobil</th>
                        <th>Paket</th>
                        <th>Paket Tambahan</th>
                        <th>Tarif</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                    
                    <tbody>
                        @forelse($tarif as $k => $v)
                            <tr>
                                <td>{{ $k+1+(($tarif->currentPage()-1)*$tarif->perPage()) }}</td>
                                <td>
                                    <img src="{{ url('/uploads/img/carsdocument') . '/' . $v->car->carDocument()->where('document_type', 'photo')->first()->file_name}}" width="50">
                                    {{strtoupper($v->car->vendor)}}
                                    {{strtoupper($v->car->tipe)}}
                                    ({{strtoupper($v->car->police_number)}})
                                </td>
                                <td>
                                    {{strtoupper(str_replace('_', ' ', $v->paket->jenis))}}<br>
                                    @if($v->paket->jenis == 'normal')
                                        <i class="fa fa-car" style="color:orange;"></i>&nbsp; {{$v->paket->jam}} Jam, {{$v->paket->jarak}} Km ({{$v->paket->keterangan}})
                                    @elseif($v->paket->jenis == 'jemput')
                                        <i class="fa fa-plane" style="color:blue;"></i>&nbsp; {{$v->airport->name}} ({{$v->airport->code}})
                                    @elseif($v->paket->jenis == 'grab_my_car')
                                        <i class="fa fa-taxi" style="color:green;"></i>&nbsp; {{strtoupper($v->paket->keterangan)}}
                                        ({{strtoupper($v->paket->tempo)}})
                                    @endif
                                </td>
                                <td>
                                    Sopir: {!!$v->sopir != null ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-remove" style="color:red"></i>'!!}<br>
                                    BBM: {!!$v->bbm != null ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-remove" style="color:red"></i>'!!}<br>
                                    Pulang Pergi: {!!$v->pp != null ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-remove" style="color:red"></i>'!!}<br>
                                    Tol: {!!$v->tol != null ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-remove" style="color:red"></i>'!!}<br>
                                    Parkir: {!!$v->parkir != null ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-remove" style="color:red"></i>'!!}
                                </td>
                                <td><strong>Rp. {{number_format($v->price,0,',','.')}}</strong></td>
                                <td>{{$v->keterangan}}</td>
                                <td><a href="{{url('mobilku/tarif/detail/' . \Hashids::connection('tarif')->encode($v->id))}}" class="btn btn-default"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            @empty
                        @endforelse
                    </tbody>
                </table>
                <center>{{$tarif->links()}}</center>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#normal" aria-controls="normal" role="tab"
                                                              data-toggle="tab">Normal</a></li>
                    <li role="presentation"><a href="#jemput" aria-controls="jemput" role="tab"
                                               data-toggle="tab">Jemput</a></li>
                    <li role="presentation"><a href="#grab_my_car" aria-controls="grab_my_car" role="tab"
                                               data-toggle="tab">Grab My Car</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="normal">
                        <br>
                        <br>
                        <fieldset>
                            {{--<legend>Set Tarif Sewa Normal</legend>--}}
                            <form role="form" class="form-horizontal" method="post" action="">
                                <div class="form-group">
                                    <label for="mobil" class="control-label col-md-3">Mobil</label>
                                    <div class="col-md-6">
                                        <select name="mobil" id="mobil" class="form-control" required>
                                            <option></option>
                                            @forelse($mobil as $k => $v)
                                                <option value="{{\Hashids::connection('mobil')->encode($v->id)}}">{{strtoupper($v->vendor . ' - ') . ucwords($v->tipe) . strtoupper(' (' . $v->police_number . ' )')}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="paket" class="control-label col-md-3">Paket</label>
                                    <div class="col-md-6">
                                        <select name="paket" id="paket" class="form-control" required>
                                            @forelse($paket as $k => $v)
                                                @if($v->jenis == 'normal')
                                                    <option value="{{\Hashids::connection('paket')->encode($v->id)}}">{{$v->jam}}
                                                        Jam / {{$v->jarak}}Km
                                                    </option>
                                                @endif
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tarif" class="control-label col-md-3">Tarif</label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon2">Rp. </span>
                                            <input type="number" name="price" min="1" step="1" class="form-control" value="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tambahan" class="control-label col-md-3">Paket Tambahan</label>
                                    <div class="col-md-6">
                                        <input type="checkbox" name="bbm">&nbsp; <i class="fa fa-tint"></i> &nbsp;
                                        BBM<br>
                                        <input type="checkbox" name="tol">&nbsp; <i class="fa fa-road"></i> &nbsp;
                                        Tol<br>
                                        <input type="checkbox" name="sopir">&nbsp; <i class="fa fa-male"></i> &nbsp;
                                        Sopir<br>
                                        <input type="checkbox" name="parkir">&nbsp; <i class="fa fa-product-hunt"></i>
                                        &nbsp; Parkir<br>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="keterangan" class="control-label col-md-3">Keterangan</label>
                                    <div class="col-md-6">
                                        <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        {{csrf_field()}}
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-success btn-block"><i class="fa fa-floppy-o"></i>&nbsp; Simpan
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </fieldset>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="jemput">
                        <br>
                        <br>
                        <fieldset>
                            {{--<legend>Set Tarif Sewa Normal</legend>--}}
                            <form role="form" class="form-horizontal" method="post" action="">
                                <div class="form-group">
                                    <label for="mobil" class="control-label col-md-3">Mobil</label>
                                    <div class="col-md-6">
                                        <select name="mobil" id="mobil" class="form-control" required @if(!is_array($airport)) disabled @endif>
                                            <option></option>
                                            @forelse($mobil as $k => $v)
                                                <option value="{{\Hashids::connection('mobil')->encode($v->id)}}">{{strtoupper($v->vendor . ' - ') . ucwords($v->tipe) . strtoupper(' (' . $v->police_number . ' )')}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bandara" class="control-label col-md-3">Bandara</label>
                                    <div class="col-md-6">
                                        @if(is_array($airport))
                                            <select name="bandara" id="paket" class="form-control" required>
                                                @foreach($airport as $v)
                                                    <option value="{{\Hashids::connection('airport')->encode($v['id'])}}">{{$v['name']}}
                                                        ({{$v['code']}})
                                                    </option>
                                                @endforeach
                                            </select>
                                        @else
                                            <select class="form-control" disabled>
                                                <option>Tidak ada bandara terdekat dari lokasi anda</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pp" class="control-label col-md-3">Pulang Pergi</label>
                                    <div class="col-md-6">
                                        <input type="checkbox" name="pp"> Pulang-Pergi
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tarif" class="control-label col-md-3">Tarif</label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon2">Rp. </span>
                                            <input type="number" name="price" min="1" step="1" class="form-control" value="" required @if(!is_array($airport)) disabled @endif>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tambahan" class="control-label col-md-3">Paket Tambahan</label>
                                    <div class="col-md-6">
                                        <input type="checkbox" name="bbm">&nbsp; <i class="fa fa-tint"></i> &nbsp;
                                        BBM<br>
                                        <input type="checkbox" name="tol">&nbsp; <i class="fa fa-road"></i> &nbsp;
                                        Tol<br>
                                        <input type="checkbox" name="sopir">&nbsp; <i class="fa fa-male"></i> &nbsp;
                                        Sopir<br>
                                        <input type="checkbox" name="parkir">&nbsp; <i class="fa fa-product-hunt"></i>
                                        &nbsp; Parkir<br>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="keterangan" class="control-label col-md-3">Keterangan</label>
                                    <div class="col-md-6">
                                        <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        {{csrf_field()}}
                                        <input type="hidden" name="paket" value="{{\Hashids::connection('paket')->encode('8')}}">
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-success btn-block" @if(!is_array($airport)) disabled @endif><i class="fa fa-floppy-o"></i>&nbsp; Simpan
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </fieldset>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="grab_my_car">
                        <br>
                        <br>
                        <fieldset>
                            {{--<legend>Set Tarif Sewa Normal</legend>--}}
                            <form role="form" class="form-horizontal" method="post" action="">
                                <div class="form-group">
                                    <label for="mobil" class="control-label col-md-3">Mobil</label>
                                    <div class="col-md-6">
                                        <select name="mobil" id="mobil" class="form-control" required @if(!is_array($airport)) disabled @endif>
                                            <option></option>
                                            @forelse($mobil as $k => $v)
                                                <option value="{{\Hashids::connection('mobil')->encode($v->id)}}">{{strtoupper($v->vendor . ' - ') . ucwords($v->tipe) . strtoupper(' (' . $v->police_number . ' )')}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="paket" class="control-label col-md-3">Paket</label>
                                    <div class="col-md-6">
                                        <select name="paket" id="paket" class="form-control" required>
                                            @forelse($paket as $k => $v)
                                                @if($v->jenis == 'grab_my_car')
                                                    <option value="{{\Hashids::connection('paket')->encode($v->id)}}">{{strtoupper($v->tempo)}}</option>
                                                @endif
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tarif" class="control-label col-md-3">Tarif</label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon2">Rp. </span>
                                            <input type="number" name="price" min="1" step="1" class="form-control" value="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="keterangan" class="control-label col-md-3">Keterangan</label>
                                    <div class="col-md-6">
                                        <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        {{csrf_field()}}
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-success btn-block" @if(!is_array($airport)) disabled @endif><i class="fa fa-floppy-o"></i>&nbsp; Simpan
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js_scripts')
@endsection