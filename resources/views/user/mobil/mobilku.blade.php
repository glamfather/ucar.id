@extends('layouts.user-dashboard')
@section('htmlheader_title', 'Mobilku')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">

@endsection

@section('contentheader_title', '<i class="fa fa-car"></i>&nbsp; Mobilku')

@section('breadcrumb')
    <li class=""><a href="/dashboard"><i class="fa fa-dashboard"></i>&nbsp; Dahsboard</a></li>
    <li class="active"><i class="fa fa-car"></i>&nbsp; Mobilku</li>
@endsection


@section('main_content')
<div class="box">
	<div class="box-header">
		<a href="/mobilku/tambah" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp; Tambah Mobil</a>
	</div>
	<div class="box-body">
		@if(Session::has('message'))
			<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
		@endif
		<table class="table table-responsive table-condensed table-hover table-striped">
			<thead>
			<tr>
                <th>No. </th>
                <th>ID Mobil</th>
				<th>Thumbnail</th>
				<th>Vendor</th>
				<th>Jenis</th>
				<th>No. Polisi</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
			</thead>
			<tbody>
			@forelse($mobil as $k => $v)
                <tr>
                    <td>{{($mobil->currentpage()-1) * $mobil->perpage() + $k + 1}}</td>
                    <td>{{\Hashids::connection('mobil')->encode($v->id)}}</td>
                    <td><img src="{{ url('/uploads/img/carsdocument') . '/' . $v->carDocument()->where('document_type', 'photo')->first()->file_name}}" width="50"></td>
                    <td>{{strtoupper($v->vendor)}}</td>
                    <td>{{strtoupper($v->tipe)}}</td>
                    <td>{{strtoupper($v->police_number)}}</td>
                    <td>{{strtoupper($v->status)}}</td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bars"></i>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('mobilku/detail/' . \Hashids::connection('mobil')->encode($v->id))}}"><i class="fa fa-eye"></i> Lihat Detail</a></li>
                                <li><a href="{{url('mobilku/tarif') .'/'. \Hashids::connection('mobil')->encode($v->id)}}"><i class="fa fa-usd"></i> Ubah Harga</a></li>
                                <li><a href="#"><i class="fa fa-circle-o-notch"></i> Ubah Status</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#"><i class="fa fa-pencil"></i> Edit</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
			@empty
			@endforelse
			</tbody>
		</table>
        <div class="">
            {{$mobil->links()}}
        </div>
	</div>
</div>

@endsection

@section('js_scripts')
@endsection