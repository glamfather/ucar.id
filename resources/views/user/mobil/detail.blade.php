@extends('layouts.user-dashboard')
@section('htmlheader_title', 'Mobilku')

@section('css')

@endsection

@section('contentheader_title', '<i class="fa fa-car"></i>&nbsp; Mobilku')

@section('breadcrumb')
    <li class=""><a href="/dashboard"><i class="fa fa-dashboard"></i>&nbsp; Dahsboard</a></li>
    <li><a href="/mobilku"> <i class="fa fa-car"></i>&nbsp; Mobilku</a></li>
    <li class="active">{{\Hashids::connection('mobil')->encode($mobil->id)}}</li>
@endsection


@section('main_content')
    <div class="box">
        <div class="box-header">
        </div>
        <div class="box-body">
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="row">
                <div class="col-md-4">
                    <div class="box">
                        <div class="box-body no-padding">
                            <img src="{{url('uploads/img/carsdocument')}}/{{$mobil->carDocument()->first()->file_name}}" style="width:100%;">
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <fieldset>
                        <dl class="dl-horizontal">
                            <dt>Vendor</dt>
                            <dd>{{strtoupper($mobil->vendor)}}</dd>
                            <dt>Tipe</dt>
                            <dd>{{strtoupper($mobil->tipe)}}</dd>
                            <dt>Tahun Rilis</dt>
                            <dd>{{strtoupper($mobil->year)}}</dd>
                            <dt>Transmisi</dt>
                            <dd>{{strtoupper($mobil->transmission_type)}}</dd>
                            <dt>Bahan Bakar</dt>
                            <dd>{{strtoupper($mobil->fuel)}}</dd>
                            <dt>No. Pilisi</dt>
                            <dd>{{strtoupper($mobil->police_number)}}</dd>
                        </dl>
                        <div>
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    
                                </thead>
                                <tr>
                                    <th>Paket</th>
                                    <th>Tarif</th>
                                    <th><i class="fa fa-bars"></i></th>
                                </tr>
                                <tbody>
                                    @forelse($tarif as $k => $v)
                                        <tr>
                                            <td>
                                                {{strtoupper(str_replace('_', ' ', $v->paket->jenis))}}<br>
                                                @if($v->paket->jenis == 'normal')
                                                    <i class="fa fa-car" style="color:orange;"></i>&nbsp; {{$v->paket->jam}} Jam, {{$v->paket->jarak}} Km ({{$v->paket->keterangan}})
                                                @elseif($v->paket->jenis == 'jemput')
                                                    <i class="fa fa-plane" style="color:blue;"></i>&nbsp; {{$v->airport->name}} ({{$v->airport->code}})
                                                @elseif($v->paket->jenis == 'grab_my_car')
                                                    <i class="fa fa-taxi" style="color:green;"></i>&nbsp; {{strtoupper($v->paket->keterangan)}}
                                                    ({{strtoupper($v->paket->tempo)}})
                                                @endif
                                            </td>
                                            <td>
                                                <strong>Rp. {{number_format($v->price,0,',','.')}}</strong>
                                            </td>
                                            <td><a href="{{url('mobilku/tarif/detail/' . \Hashids::connection('tarif')->encode($v->id))}}" class="btn btn-default"><i class="fa fa-eye"></i></a></td>
                                        </tr>
                                    @empty
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js_scripts')
@endsection