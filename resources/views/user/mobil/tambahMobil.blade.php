@extends('layouts.user-dashboard')
@section('htmlheader_title', 'Tambah Mobil')

@section('css')
@endsection

@section('contentheader_title', '<i class="fa fa-car"></i>&nbsp; Tambah Mobil')

@section('breadcrumb')
    <li class=""><i class="fa fa-dashboard"></i>&nbsp; Dahsboard</li>
    <li class=""><a href="/mobilku"><i class="fa fa-car"></i>&nbsp; Mobilku</a></li>
    <li class="active">Tambah Mobil</li>
@endsection


@section('main_content')
    <div class="box">
        <div class="box-header">
            <a href="/mobilku" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp; Mobilku</a>
        </div>
        <div class="box-body">
            <div class="col-md-6 col-md-offset-3">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form role="form" class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                    {{csrf_field()}}
                <div class="form-group">
                    <label for="vendor" class="control-label col-md-3">Vendor *</label>
                    <div class="col-md-9">
                        <input type="text" name="vendor" id="vendor" class="form-control" required placeholder="Toyota">
                        <span class="help-block">Isi dengan vendor mobil. Cth: Toyota, Suzuki, Yamaha, dll.</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tipe" class="control-label col-md-3">Tipe *</label>
                    <div class="col-md-9">
                        <input type="text" name="tipe" id="tipe" class="form-control" required placeholder="Kijang">
                        <span class="help-block">Isi dengan tipe mobil. Cth: Innova, Avanzac, dll.</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="police_number" class="control-label col-md-3">Nomor Polisi *</label>
                    <div class="col-md-9">
                        <input type="text" name="police_number" id="police_number" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tahun" class="control-label col-md-3">Tahun Rilis</label>
                    <div class="col-md-9">
                        <select name="tahun" id="tahun" class="form-control">
                            @for($i=date('Y'); $i >= date('Y')-30; $i--)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="color" class="control-label col-md-3">Transmisi *</label>
                    <div class="col-md-9">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="transmission_type" autocomplete="off" value="matic" checked required="required">
                                Matic
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="transmission_type" autocomplete="off" value="manual" required="required"> Manual
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="color" class="control-label col-md-3">Bahan Bakar</label>
                    <div class="col-md-9">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="bbm" value="bbm" autocomplete="off" checked> BBM
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="bbm" value="non_bbm" autocomplete="off"> Non BBM
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="color" class="control-label col-md-3">Gambar 1 *</label>
                    <div class="col-md-9">
                        <input type="file" name="gambar_utama" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label for="color" class="control-label col-md-3">Gambar 2</label>
                    <div class="col-md-9">
                        <input type="file" name="gambar[]">
                    </div>
                </div>
                <div class="form-group">
                    <label for="color" class="control-label col-md-3">Gambar 3</label>
                    <div class="col-md-9">
                        <input type="file" name="gambar[]">
                    </div>
                </div>
                <div class="form-group">
                    <label for="color" class="control-label col-md-3">Gambar 4</label>
                    <div class="col-md-9">
                        <input type="file" name="gambar[]">
                    </div>
                </div>
                <div class="form-group">
                    <label for="keterangan" class="control-label col-md-3">Keterangan</label>
                    <div class="col-md-9">
                        <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-primary pull-right">Submit <i class="fa fa-save"></i>
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection