@extends('layouts.user-dashboard')
@section('htmlheader_title', 'Setting')

@section('css')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuzwI2s2bf2lDglKewNMEvqeF_nqVvouE&libraries=places"></script>

@endsection

@section('contentheader_title', '<i class="fa fa-cogs"></i>&nbsp; Setting')

@section('breadcrumb')
	<li class=""><a href="/dashboard"><i class="fa fa-dashboard"></i>&nbsp; Dahsboard</a></li>
	<li class=""><a href="/profile"><i class="fa fa-user"></i>&nbsp; Profile</a></li>
    <li class="active"><i class="fa fa-cogs"></i>&nbsp; Setting</li>
@endsection


@section('main_content')
<div class="box">
	<div class="box-header">
		{{--<a href="/profile/setting" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp; Edit</a>--}}
	</div>
	<div class="box-body">
		{{--<div class="col-md-3">--}}
		{{--</div>--}}
		<div class="col-md-9">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if($user->verified == 0)
                <div class="alert alert-info">
                    <ul>
                        <li>Akun anda belum terverifikasi, silahkan cek email anda untuk memverifikasi.</li>
                        @if($user->approved == 0)
                            <li>Akun anda belum disetujui, silahkan <a href="/dokumenku">upload</a> dokumen Identitas anda.</li>
                        @endif
                    </ul>
                </div>
            @endif
            <form role="form" class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="id" class="control-label col-md-3">User ID</label>
                    <div class="col-md-9">
                        <input type="text" name="id" id="id" class="form-control" value="{{$user_id}}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="control-label col-md-3">Nama Lengkap *</label>
                    <div class="col-md-9">
                        <input type="text" name="name" id="name" class="form-control" value="{{$user->name}}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="control-label col-md-3">Email *</label>
                    <div class="col-md-9">
                        <input type="email" name="email" id="email" class="form-control" value="{{$user->email}}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone1" class="control-label col-md-3">No. Telp 1 *</label>
                    <div class="col-md-9">
                        <input type="tel" name="phone1" id="phone1" class="form-control" value="{{$user->phone1}}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone2" class="control-label col-md-3">No. Telp 2 (optional)</label>
                    <div class="col-md-9">
                        <input type="tel" name="phone2" id="phone2" class="form-control" value="{{$user->phone2}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat" class="control-label col-md-3">Alamat</label>
                    <div class="col-md-9">
                        <textarea name="alamat" id="alamat" class="form-control" required>{{$user->alamat}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lokasi" id="lat-long" class="control-label col-md-3">Lokasi *</label>
                    <div class="col-md-9">
                        <div class="input-group input-daterange">
                            <span class="input-group-addon">Latitude</span>
                            <input type="text" class="form-control" id="lat" value="{{$user->latitude}}" disabled="disabled">
                            <span class="input-group-addon">Longitude</span>
                            <input type="text" class="form-control" id="lng" value="{{$user->longitude}}" disabled="disabled">
                            <input type="hidden" name="latitude" id="latitude" class="form-control" value="{{$user->latitude}}" required>
                            <input type="hidden" name="longitude" id="longitude" class="form-control" value="{{$user->longitude}}" required>
                        </div>
                        <br>
                        <input type="text" name="searchmap" id="searchmap" class="form-control input-sm" value="" placeholder="Temukan lokasi anda">
                        <div class="" id="map-canvas" style="height: 300px; width: 572px;"></div>
                        {{-- <input type="email" name="email" id="email" class="form-control" value="{{$user->email}}" required> --}}
                    </div>
                </div>
{{--                 <div class="form-group">
                    <label for="provinsi" class="control-label col-md-3">Provinsi</label>
                    <div class="col-md-9">
                        <select name="provinsi" id="provinsi" class="form-control" required>
                                @foreach($provinsi as $k => $v)
                                    <option value="{{$v->id}}"@if($v->id == $user->provinsi) selected="selected"@endif>{{$v->name}}</option>
                                @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kabupaten" class="control-label col-md-3">Kabupaten/Kota</label>
                    <div class="col-md-9">
                        <select name="kabupaten" id="kabupaten" class="form-control" required>
                            <option>Pilih Kabupaten/Kota</option>
                            @if($user->provinsi != null)
                                @foreach($indonesia->cities as $k => $v)
                                    <option value="{{$v->id}}"@if($v->id == $user->kabupaten) selected="selected"@endif>{{$v->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kecamatan" class="control-label col-md-3">Kecamatan</label>
                    <div class="col-md-9">
                        <select name="kecamatan" id="kecamatan" class="form-control" required>
                            <option>Pilih Kecamatan</option>
                            @if($user->provinsi != null)
                                @foreach($indonesia->districts as $k => $v)
                                    <option value="{{$v->id}}"@if($v->id == $user->kecamatan) selected="selected"@endif>{{$v->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="desa" class="control-label col-md-3">Desa/Kelurahan</label>
                    <div class="col-md-9">
                        <select name="desa" id="desa" class="form-control" required>
                            <option>Pilih Desa/Kelurahan</option>
                            @if($user->kelurahan != null)
 --}}                                <?php
                                    //$desa = Laravolt\Indonesia\Models\Village::where('district_id', $user->kecamatan)->get();
                                ?>
{{--                                 @foreach($desa as $k => $v)
                                    <option value="{{$v->id}}"@if($v->id == $user->kelurahan) selected="selected"@endif>{{$v->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div> --}}

                <div class="form-group">
                    <label for="photo" class="control-label col-md-3">Photo</label>
                    <div class="col-md-9">
                        <img src="{{asset('uploads/img/userphoto') .'/'. $user->photo}}" class="img img-rounded" id="photoPref" width="150" height="150" alt="PHOTO">
                        <br>
                        <input type="file" name="photo" id="photo">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-success pull-right">Simpan <i class="fa fa-floppy-o"></i></button>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>

@endsection

@section('js_scripts')

<script type="text/javascript">
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#photoPref').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#photo").change(function(){
        readURL(this);
    });
    $(document).ready(function() {
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });

    var map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: {lat: {{($user->latitude) ? $user->latitude : '-6.177810'}}, lng: {{($user->longitude) ? $user->longitude : '106.8260873'}} },
          zoom: {{($user->latitude) ? '16' : '8'}}
        });

    var marker = new google.maps.Marker({
        position: { lat: {{($user->latitude) ? $user->latitude : '-6.177810'}}, lng: {{($user->longitude) ? $user->longitude : '106.8260873'}} },
        map: map,
        draggable: true
    });

    var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

    google.maps.event.addListener(searchBox, 'places_changed', function() {
        var places = searchBox.getPlaces();
        var bounds = new google.maps.LatLngBounds();
        var i, place;
        for(i=0; place=places[i];i++){
            bounds.extend(place.geometry.location);
            marker.setPosition(place.geometry.location);
        }

        map.fitBounds(bounds);
        map.setZoom(15);
    });

    google.maps.event.addListener(marker, 'position_changed', function(){
        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();

        $('#lat, #latitude').val(lat);
        $('#lng, #longitude').val(lng);
    })


    //    var infoWindow = new google.maps.InfoWindow({map: map});
    //
    //    // Try HTML5 geolocation.
    //    if (navigator.geolocation) {
    //        navigator.geolocation.getCurrentPosition(function(position) {
    //            var pos = {
    //                lat: position.coords.latitude,
    //                lng: position.coords.longitude
    //            };
    //
    //            infoWindow.setPosition(pos);
    //            infoWindow.setContent('Location found.');
    //            map.setCenter(pos);
    //        }, function() {
    //            handleLocationError(true, infoWindow, map.getCenter());
    //        });
    //    } else {
    //        // Browser doesn't support Geolocation
    //        handleLocationError(false, infoWindow, map.getCenter());
    //    }
    //
    //    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    //        infoWindow.setPosition(pos);
    //        infoWindow.setContent(browserHasGeolocation ?
    //                'Error: The Geolocation service failed.' :
    //                'Error: Your browser doesn\'t support geolocation.');
    //    }
</script>
{{--     <script>
        $("#provinsi").change(function () {
           var provinsi = $(this).val();
            $.ajax({
                url : '{{url('provinceWithCities')}}/' + provinsi,
                type : 'GET',
                dataType : 'text',
                beforeSend: function () {
                    $('#kabupaten').attr('disabled', true);
                    $('#kecamatan').attr('disabled', true);
                    $('#desa').attr('disabled', true);
                },
                success : function (data) {
                    $('#kabupaten').html(data).attr('disabled', false);
                    $('#kecamatan').html('<option>--Pilih Kecamatan--</option>').attr('disabled', false);
                    $('#desa').html('<option>--Pilih Desa/Kelurahan--</option>').attr('disabled', false);
                },
                error: function () {

                }
            });
        });
        $("#kabupaten").change(function () {
            var kabupaten = $(this).val();
            $.ajax({
                url : '{{url('cityWithDistricts')}}/' + kabupaten,
                type : 'GET',
                dataType : 'text',
                beforeSend: function () {
                    $('#kecamatan').attr('disabled', true);
                    $('#desa').attr('disabled', true);
                },
                success : function (data) {
                    $('#kecamatan').html(data).attr('disabled', false);
                    $('#desa').html('<option>--Pilih Desa/Kelurahan--</option>').attr('disabled', false);
                },
                error: function () {

                }
            });
        });
        $("#kecamatan").change(function () {
            var kecamatan = $(this).val();
            $.ajax({
                url : '{{url('districtWithVillages')}}/' + kecamatan,
                type : 'GET',
                dataType : 'text',
                beforeSend: function () {
                    $('#desa').attr('disabled', true);
                },
                success : function (data) {
                    $('#desa').html(data).attr('disabled', false);
                },
                error: function () {

                }
            });
        });
    </script> --}}
@endsection