@extends('layouts.user-dashboard')
@section('htmlheader_title', 'Profile')

@section('css')
@endsection

@section('contentheader_title', '<i class="fa fa-user"></i>&nbsp; Profile')

@section('breadcrumb')
    <li class=""><a href="/dashboard"><i class="fa fa-dashboard"></i>&nbsp; Dahsboard</a></li>
    <li class="active"><i class="fa fa-user"></i>&nbsp; Profile</li>
@endsection


@section('main_content')
<div class="box">
	<div class="box-header">
		<a href="/profile/setting" class="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp; Edit</a>
	</div>
	<div class="box-body">
		<div class="col-md-3">
			<img src="{{asset('uploads/img/userphoto') .'/'. $user->photo}}" class="img img-rounded" width="150" height="150" alt="PHOTO">
		</div>
		<div class="col-md-9">
			@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<dl class="dl-horizontal">
				<dt>ID</dt>
					<dd>{{\Hashids::encode($user->id)}}</dd>
				<dt>Nama</dt>
					<dd>{{$user->name}}</dd>
				<dt>Email</dt>
					<dd>{{$user->email}}</dd>
				<dt>Tipe</dt>
					<dd>{!!($user->type == 'user') ? '<i class="fa fa-user"></i> User' : '<i class="fa fa-star" style="color:orange;"></i> Admin'!!}</dd>
				<dt>Status Approval</dt>
					<dd>{!!($user->approved == 0) ? '<i class="fa fa-times-circle" style="color:orange;"></i> &nbsp;Unapproved' : '<i class="fa fa-check" style="color:green;"></i> &nbsp;Approved'!!}</dd>
				<dt>Email Verifikasi</dt>
					<dd>{!!($user->verified == 0) ? '<i class="fa fa-clock-o"></i> &nbsp;Unverified' : '<i class="fa fa-check" style="color:green;"></i> &nbsp;Verified'!!}</dd>
				<dt>Alamat</dt>
					<dd>{!!$user->alamat . '<br>' . $user->kelurahan . '-' . $user->kecamatan . '-' . $user->kabupaten . '<br>' . $user->provinsi !!}</dd>
			</dl>
        </div>
	</div>
</div>

@endsection

@section('js_scripts')
@endsection