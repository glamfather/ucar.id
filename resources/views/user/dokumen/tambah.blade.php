@extends('layouts.user-dashboard')
@section('htmlheader_title', 'Dokumenku')

@section('css')
@endsection

@section('contentheader_title', '<i class="fa fa-file"></i>&nbsp; Dokumenku')

@section('breadcrumb')
    <li class=""><a href="{{url('dashboard)}}"> <i class="fa fa-dashboard"></i>&nbsp; Dahsboard</a></li>
    <li><a href="/dokumenku"><i class="fa fa-file"></i>&nbsp; Dokumenku</a></li>
    <li class="active">Tambah Dokumen</li>
@endsection


@section('main_content')
    <div class="box">
        <div class="box-header">
            <a href="/dokumenku" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp; Dokumen</a>
        </div>
        <div class="box-body">
            <div class="col-md-9">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form role="form" class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="ktp" class="control-label col-md-3">KTP</label>
                    <div class="col-md-9">
                        <img src="{{asset('uploads/img/userdocument')}}/default.png" class="img img-rounded" id="ktpPref" width="20" height="20" alt="PHOTO">
                        <br>
                        <input type="file" name="ktp" id="ktp" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kk" class="control-label col-md-3">Kartu Keluarga (KK)</label>
                    <div class="col-md-9">
                        <img src="{{asset('uploads/img/userdocument')}}/default.png" class="img img-rounded" id="kkPref" width="20" height="20" alt="PHOTO">
                        <br>
                        <input type="file" name="kk" id="kk" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="skck" class="control-label col-md-3">Surat Keterangan Catatan Kepolisian (SKCK)</label>
                    <div class="col-md-9">
                        <img src="{{asset('uploads/img/userdocument')}}/default.png" class="img img-rounded" id="skckPref" width="20" height="20" alt="PHOTO">
                        <br>
                        <input type="file" name="skck" id="skck" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sim" class="control-label col-md-3">Surat Izin Mengemudi (SIM)</label>
                    <div class="col-md-9">
                        <img src="{{asset('uploads/img/userdocument')}}/default.png" class="img img-rounded" id="simPref" width="20" height="20" alt="PHOTO">
                        <br>
                        <input type="file" name="sim" id="sim" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-success pull-right">Simpan <i class="fa fa-floppy-o"></i></button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

@endsection

@section('js_scripts')
<script>
    function readURL(input, target) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                target.attr('src', e.target.result).attr('width', 'auto').attr('height', 100);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#ktp").change(function(){
        readURL(this, $('#ktpPref'));
    });
    $("#kk").change(function(){
        readURL(this, $('#kkPref'));
    });
    $("#skck").change(function(){
        readURL(this, $('#skckPref'));
    });
    $("#sim").change(function(){
        readURL(this, $('#simPref'));
    });
</script>
@endsection