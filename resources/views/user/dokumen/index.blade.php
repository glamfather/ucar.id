@extends('layouts.user-dashboard')
@section('htmlheader_title', 'Dokumenku')

@section('css')
@endsection

@section('contentheader_title', '<i class="fa fa-file"></i>&nbsp; Dokumenku')

@section('breadcrumb')
    <li class=""><i class="fa fa-dashboard"></i>&nbsp; Dahsboard</a></li>
    <li class="active"><i class="fa fa-file"></i>&nbsp; Dokumenku</li>
@endsection


@section('main_content')
<div class="box">
	<div class="box-header">
        <a href="/dokumenku/tambah" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp; Tambah Dokumen</a>
	</div>
	<div class="box-body">
		<table class="table table-responsive table-condensed table-hover table-stripped">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>Thumbnail</th>
                    <th>Jenis Dokumen</th>
                    <th>File</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($dokumen as $k => $v)
                    <tr>
                        <td>{{$k+1}}</td>
                        <td><img src="{{asset('uploads/img/userdocument') . '/' . $v->file_name}}" width="50" height="50"></td>
                        <td>{{strtoupper($v->document_type)}}</td>
                        <td>{{$v->file_name}}</td>
                        <td></td>
                        <td>{{$v->id}}</td>
                    </tr>
                @empty
                @endforelse
            </tbody>
        </table>
	</div>
</div>

@endsection

@section('js_scripts')
@endsection