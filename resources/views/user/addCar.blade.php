
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tambah Mobil</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/paper.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://code.jquery.com/jquery-2.2.3.min.js"
            integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <style>
        body {
            margin-top: 20px;

            background: #ECE9E6; /* fallback for old browsers */
            background: -webkit-linear-gradient(to left, #ECE9E6, #FFFFFF); /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to left, #ECE9E6, #FFFFFF); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        }

        .map-responsive {
            overflow: hidden;
            padding-bottom: 56.25%;
            position: relative;
            height: 0;
        }

        .map-responsive iframe {
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            position: absolute;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="col-md-3 hidden-sm hidden-xs">
                <div class="sidebar-nav-fixed affix panel">
                    <ul class="nav">
                        <li style="padding:0;">
                            <img src="uploads/img/userphoto/default.png" class="img-rounded" width="200">
                        </li>
                        <a href="#skills" class="list-group-item">
                            <h6 class="list-group-item-heading">
                                <i class="fa fa-refresh"></i>&nbsp; Transaksi
                            </h6>
                        </a>
                        <a href="#portfolio" class="list-group-item">
                            <h6 class="list-group-item-heading">
                                <i class="fa fa-envelope"></i>&nbsp; Pesan
                            </h6>
                        </a>
                        <a href="#education" class="list-group-item">
                            <h6 class="list-group-item-heading">
                                <i class="fa fa-car"></i>&nbsp; Mobil
                            </h6>
                        </a>
                        <a href="#portfolio" class="list-group-item">
                            <h6 class="list-group-item-heading">
                                <i class="fa fa-file"></i>&nbsp; Dokumen
                            </h6>
                        </a>
                        <a href="#portfolio" class="list-group-item">
                            <h6 class="list-group-item-heading">
                                <i class="fa fa-cogs"></i>&nbsp; Pengaturan
                            </h6>
                        </a>
                    </ul>
                </div>
                <!--/sidebar-nav-fixed -->
            </div>
            <!--/span-->
            <div class="col-md-9">

                <section id="skills" class="well">
                    <fieldset>
                        <legend>
                            <h5><i class="fa fa-car"></i>&nbsp; Tambah Mobil</h5>
                        </legend>
                        <form role="form" class="form-horizontal" method="post" action="">
                            <div class="form-group">
                                <label for="manufacturer" class="control-label col-md-3">Vendor</label>
                                <div class="col-md-9">
                                    <select name="manufacturer" id="manifacturer" class="form-control" required="required">
                                        <option value="">Toyota</option>
                                        <option value="">Honda</option>
                                        <option value="">Hyundai</option>
                                        <option value="">Kia</option>
                                        <option value="">Mitsubishi</option>
                                        <option value="">Suzuki</option>
                                        <option value="">Yamaha</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="color" class="control-label col-md-3">Warna</label>
                                <div class="col-md-9">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default" style="background: #000;color: #eee;">
                                            <input type="checkbox" autocomplete="off" checked> Hitam
                                        </label>
                                        <label class="btn btn-default" style="background: red;color: #eee;">
                                            <input type="checkbox" autocomplete="off"> Merah
                                        </label>
                                        <label class="btn btn-default" style="background: #fff;color: #777;">
                                            <input type="checkbox" autocomplete="off"> Putih
                                        </label>
                                        <label class="btn btn-default" style="background: #c0c0c0;color: #777;">
                                            <input type="checkbox" autocomplete="off"> Perak
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="color" class="control-label col-md-3">Transmisi</label>
                                <div class="col-md-9">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default">
                                            <input type="checkbox" autocomplete="off" checked> Matic
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" autocomplete="off"> Manual
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="color" class="control-label col-md-3">Bahan Bakar</label>
                                <div class="col-md-9">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default">
                                            <input type="checkbox" autocomplete="off" checked> Premium
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" autocomplete="off"> Pertamax
                                        </label>
                                        <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default">
                                            <input type="checkbox" autocomplete="off" checked> Solar
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" autocomplete="off"> Elektrik
                                        </label>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="color" class="control-label col-md-3">Gambar 1</label>
                                <div class="col-md-9">
                                    <input type="file" name="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="color" class="control-label col-md-3">Gambar 2</label>
                                <div class="col-md-9">
                                    <input type="file" name="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="color" class="control-label col-md-3">Gambar 3</label>
                                <div class="col-md-9">
                                    <input type="file" name="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="color" class="control-label col-md-3">Gambar 4</label>
                                <div class="col-md-9">
                                    <input type="file" name="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="color" class="control-label col-md-3">BPKB</label>
                                <div class="col-md-9">
                                    <input type="file" name="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="color" class="control-label col-md-3">STNK</label>
                                <div class="col-md-9">
                                    <input type="file" name="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3"></div>
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-primary pull-right">Submit <i class="fa fa-save"></i></button>
                                </div>
                            </div>
                        </form>
                </section>
               
                <footer>
                    <p>Ucar.id 2016</p>
                </footer>
            </div>
        </div>
        <!--/row-->
    </div>
</div>
<!--/.fluid-container-->
</body>
</html>
