@extends('layouts.user-dashboard')
@section('htmlheader_title', 'Transaksi')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">

@endsection

@section('contentheader_title', '<i class="fa fa-refresh"></i>&nbsp; Transaksi')

@section('breadcrumb')
<li class=""><a href="/dashboard"><i class="fa fa-dashboard"></i>&nbsp; Dahsboard</a></li>
<li class="active"><i class="fa fa-refresh"></i>&nbsp; Transaksi</li>
@endsection


@section('main_content')
<div class="box">
	<div class="box-header">
		{{-- <a href="/mobilku/tambah" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp; Tambah Mobil</a> --}}
	</div>
	<div class="box-body">
		<fieldset>
			<legend>
				<h5><i class="fa fa-refresh"></i>&nbsp; Transaksi</h5>
			</legend>
			<table class="table table-condensed table-bordered table-hover table-stripped">
				<thead>
					<tr>
						<th>No</th>
						<th>Tgl</th>
						<th>Jenis Transaksi</th>
						<th>User</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>#4937</td>
						<td>4/10/2016</td>
						<td>
							<span class="label label-primary"><i class="fa fa-level-up"></i>&nbsp; Menyewakan</span>
						</td>
						<td>
							<a href="">Jarwo Supono</a>
						</td>
						<td>
							<span class="label label-success"><i class="fa fa-check"></i>&nbsp; Sukses</span>
						</td>
						<td>
							<div class="btn-group btn-group-sm" role="group" aria-label="...">
								<a href="" class="btn btn-default"><i class="fa fa-print"></i></a>
								<a href="" class="btn btn-default"><i class="fa fa-file-o"></i></a>
								<a href="" class="btn btn-default"><i class="fa fa-eye"></i></a>
							</div>
						</td>
					</tr>
					<tr>
						<td>#4937</td>
						<td>15/10/2016</td>
						<td>
							<span class="label label-primary"><i class="fa fa-level-up"></i>&nbsp; Menyewakan</span>
						</td>
						<td>
							<a href="">Tomy Saputra</a>
						</td>
						<td>
							<span class="label label-warning"><i class="fa fa-hourglass-1"></i>&nbsp; Menunggu Pembayaran</span>
						</td>
						<td>
							<div class="btn-group btn-group-sm" role="group" aria-label="...">
								<a href="" class="btn btn-default"><i class="fa fa-print"></i></a>
								<a href="" class="btn btn-default"><i class="fa fa-file-o"></i></a>
								<a href="" class="btn btn-default"><i class="fa fa-eye"></i></a>
							</div>
						</td>

					</tr>
				</tbody>
			</table>
		</fieldset>
	</div>
</div>

@endsection

@section('js_scripts')
@endsection