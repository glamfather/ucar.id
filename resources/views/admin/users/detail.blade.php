@extends('layouts.master')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">

@endsection

@section('contentheader_title', '<i class="fa fa-users"></i>&nbsp; Management User')

@section('breadcrumb')
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#"><i class="fa fa-user"></i> Users</a></li>
    <li class="active">{{$user->name}}</li>
@endsection

@section('main_content')
<div class="box">
	<div class="box-header with-border">
		
	</div>
	<div class="box-body">
		<div class="col-md-3">
			<img src="{{asset('uploads/img/userphoto') .'/'. $user->photo}}" class="img img-rounded" width="150" height="150" alt="PHOTO">
		</div>
		<div class="col-md-9">
			<dl class="dl-horizontal">
				<dt>ID</dt>
					<dd>{{$user->id}}</dd>
				<dt>Nama</dt>
					<dd>{{$user->name}}</dd>
				<dt>Email</dt>
					<dd>{{$user->email}}</dd>
				<dt>Tipe</dt>
					<dd>{!!($user->type == 'user') ? '<i class="fa fa-user"></i> User' : '<i class="fa fa-star" style="color:orange;"></i> Admin'!!}</dd>
				<dt>Status Approval</dt>
					<dd>{!!($user->approved == 0) ? '<i class="fa fa-times-circle" style="color:orange;"></i> &nbsp;Unapproved' : '<i class="fa fa-check" style="color:green;"></i> &nbsp;Approved'!!}</dd>
				<dt>Email Verifikasi</dt>
					<dd>{!!($user->verified == 0) ? '<i class="fa fa-clock-o"></i> &nbsp;Unverified' : '<i class="fa fa-check" style="color:green;"></i> &nbsp;Verified'!!}</dd>
			</dl>	
		</div>
		

	</div>
	<div class="box-footer">
		<div class="pull-left">
            <a href="/admin/users" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
        </div>
        <div class="pull-right">
            <form method="post" action="{{url('/admin/user/' . $user->id . '/delete')}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="butn-group">
	                <button type="submit" class="btn btn-danger btn-flat" onclick="return confirm('Hapus {{$user->name}} ?')">Delete</button>
    		        <a href="/admin/user/edit/{{$user->id}}" class="btn btn-warning btn-flat">Edit</a>    	
                </div>
            </form>

        </div>
	</div>
</div>

@endsection

@section('js_scripts')
@endsection