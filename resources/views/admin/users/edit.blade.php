@extends('layouts.master')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">

@endsection

@section('contentheader_title', '<i class="fa fa-users"></i>&nbsp; Management User')

@section('breadcrumb')
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#"><i class="fa fa-user"></i> Users</a></li>
    <li class="active">Edit User</li>
@endsection


@section('main_content')
<div class="box">
	<div class="box-header with-border">
		Edit User
	</div>

	<div class="box-body">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form role="form" class="form-horizontal" action="/admin/user/update/{{$user->id}}" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="name" class="control-label col-md-2">Full Name</label>
				<div class="col-md-6">
					<input type="text" name="name" class="form-control" id="name" required="required" value="{{$user->name}}">
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="control-label col-md-2">Email</label>
				<div class="col-md-6">
					<input type="email" name="email" class="form-control" id="email" required="required" value="{{$user->email}}">
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="control-label col-md-2">Password</label>
				<div class="col-md-6">
					<input type="password" name="password" class="form-control" id="password">
				</div>
			</div>
			<div class="form-group">
				<label for="password_confirmation" class="control-label col-md-2">Retype Password</label>
				<div class="col-md-6">
					<input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
				</div>
			</div>
			<div class="form-group">
				<label for="photo" class="control-label col-md-2">Photo</label>
				<div class="col-md-6">
					<img src="{{asset('uploads/img/userphoto/' . $user->photo)}}" id="photoPref" class="img img-rounded" width="150" height="150">
					<input type="file" name="photo" id="photo" accept="image/*">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-2">
					{{csrf_field()}}
				</div>
				<div class="col-md-6">
            		<button type="submit" class="btn btn-success btn-flat pull-right">Simpan</button>
				</div>
			</div>

		</form>	

	</div>
	<div class="box-footer">
		<div class="pull-left">
            <a href="/admin/users" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
        </div>
        <div class="pull-right">
        </div>
	</div>
</div>

@endsection

@section('js_scripts')
<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#photoPref').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#photo").change(function(){
    readURL(this);
});
</script>
@endsection