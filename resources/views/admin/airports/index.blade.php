@extends('layouts.master')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">

@endsection

@section('contentheader_title', '<i class="fa fa-plane"></i>&nbsp; Management Airport')

@section('breadcrumb')
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Airport</li>
@endsection

@section('main_content')
<div class="box">
	<div class="box-header with-border">
		
	</div>
	<div class="box-body">
		<div class="box-header">
			<a href="/admin/airport/create" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp; Add new Airport</a>
		</div>
		{!! $dataTable->table() !!}
		

	</div>
	<div class="box-footer"></div>
</div>

@endsection

@section('js_scripts')
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}
@endsection