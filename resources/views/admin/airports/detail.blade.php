@extends('layouts.master')

@section('css')
	<style type="text/css">
		#map {
        height: 100%;
      }
	</style>
@endsection

@section('contentheader_title', '<i class="fa fa-plane"></i>&nbsp; Management Airport')

@section('breadcrumb')
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{url('admin/airport/')}}"><i class="fa fa-plane"></i> Airports</a></li>
    <li class="active">{{$airport->name}}</li>
@endsection

@section('main_content')
<div class="box">
	<div class="box-header with-border">
		
	</div>
	<div class="box-body">
		<div class="col-md-9">
			<dl class="dl-horizontal">
				<dt>ID</dt>
					<dd>{{$airport->id}}</dd>
				<dt>Airport Code</dt>
					<dd>{{$airport->code}}</dd>
				<dt>Name</dt>
					<dd>{{$airport->name}}</dd>
				<dt>City</dt>
					<dd>{{$airport->cityName . ' (' . $airport->cityCode . ')'}}</dd>
				<dt>Latitude</dt>
					<dd>{{$airport->lat}}</dd>
				<dt>Longitude</dt>
					<dd>{{$airport->lon}}</dd>
			</dl>

		</div>
	</div>
	<div class="box-footer">
		<div class="pull-left">
            <a href="/admin/airports" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
        </div>
        <div class="pull-right">
            <form method="post" action="{{url('/admin/airport/delete/' . $airport->id)}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="butn-group">
	                <button type="submit" class="btn btn-danger btn-flat" onclick="return confirm('Hapus {{$airport->name}} ?')">Delete</button>
    		        <a href="/admin/airport/edit/{{$airport->id}}" class="btn btn-warning btn-flat">Edit</a>    	
                </div>
            </form>

        </div>
	</div>


</div>

@endsection

@section('js_scripts')

{{-- <script type="text/javascript">
var map;
	function initMap() {
       map = new google.maps.Map(document.getElementById('map'), {
  center: {lat: -34.397, lng: 150.644},
  zoom: 8
});
      }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvLB1MH-w-n8QBkZ_Bb7iH1YYJYMeOUHg&callback=initMap" async defer></script>
 --}}
@endsection