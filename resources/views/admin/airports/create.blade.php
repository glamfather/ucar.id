@extends('layouts.master')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">

@endsection

@section('contentheader_title', '<i class="fa fa-users"></i>&nbsp; Management User')

@section('breadcrumb')
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#"><i class="fa fa-user"></i> Users</a></li>
    <li class="active">Create New User</li>
@endsection

@section('main_content')
<div class="box">
	<div class="box-header with-border">
		Create New User
	</div>
	<div class="box-body">
		<form role="form" class="form-horizontal" action="/admin/user/store" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="name" class="control-label col-md-2">Full Name</label>
				<div class="col-md-6">
					<input type="text" name="name" class="form-control" id="name" required="required">
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="control-label col-md-2">Email</label>
				<div class="col-md-6">
					<input type="email" name="email" class="form-control" id="email" required="required">
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="control-label col-md-2">Password</label>
				<div class="col-md-6">
					<input type="password" name="password" class="form-control" id="password" required="required">
				</div>
			</div>
			<div class="form-group">
				<label for="repassword" class="control-label col-md-2">Retype Password</label>
				<div class="col-md-6">
					<input type="password" name="repassword" class="form-control" id="repassword" required="required">
				</div>
			</div>
			<div class="form-group">
				<label for="photo" class="control-label col-md-2">Photo</label>
				<div class="col-md-6">
					<img src="{{asset('uploads/img/userphoto/default.png')}}" id="photoPref" class="img img-rounded" width="150" height="150">
					<input type="file" name="photo" id="photo" accept="image/*">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-2">
					{{csrf_field()}}
				</div>
				<div class="col-md-6">
            		<button type="submit" class="btn btn-success btn-flat pull-right">Simpan</button>
				</div>
			</div>

		</form>	

	</div>
	<div class="box-footer">
		<div class="pull-left">
            <a href="/admin/users" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
        </div>
        <div class="pull-right">
        </div>
	</div>
</div>

@endsection

@section('js_scripts')
<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#photoPref').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#photo").change(function(){
    readURL(this);
});
</script>
@endsection