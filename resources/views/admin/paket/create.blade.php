@extends('layouts.master')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">

@endsection

@section('contentheader_title', '<i class="fa fa-gift"></i>&nbsp; Tambah Paket')

@section('breadcrumb')
<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="#"><i class="fa fa-gift"></i> Paket</a></li>
<li class="active">Tambah Paket</li>
@endsection


@section('main_content')
<div class="box">
	<div class="box-header with-border">
		Tambah Paket
	</div>

	<div class="box-body">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif


		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#normal" aria-controls="normal" role="tab" data-toggle="tab">Normal</a></li>
			<li role="presentation"><a href="#jemput" aria-controls="jemput" role="tab" data-toggle="tab">Jemput</a></li>
			<li role="presentation"><a href="#grab_my_car" aria-controls="grab_my_car" role="tab" data-toggle="tab">Grab My Car</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="normal">
				<div class="col-md-12">
					<br>
					<br>
					<form role="form" class="form-horizontal" action="/admin/paket/store" method="post">
						<div class="form-group">
							<label for="hour" class="control-label col-md-2">Waktu</label>
							<div class="col-md-2">
								<div class="input-group">
									<input type="number" min="1" step="1" name="hour" class="form-control" id="hour" required="required">
									<span class="input-group-addon">Jam</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="range" class="control-label col-md-2">Jarak</label>
							<div class="col-md-2">
								<div class="input-group">
									<input type="number" min="1" step="1" name="range" class="form-control" id="range" required="required">
									<span class="input-group-addon">Km</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="keterangan" class="control-label col-md-2">Keterangan</label>
							<div class="col-md-6">
								<input type="keterangan" name="keterangan" class="form-control" id="keterangan" placeholder="Dalam Kota" required="required">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-2">
								{{csrf_field()}}
							</div>
							<div class="col-md-6">
								<button type="submit" class="btn btn-success btn-flat pull-right">Simpan</button>
							</div>
						</div>

					</form>	
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="jemput">
				<div class="col-md-12">
					<br>
					<br>
					<div class="alert alert-info">
						<i class="fa fa-check"></i> Tidak ada paket untuk jemput
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="grab_my_car">
				<div class="col-md-12">
					<br>
					<br>
					<form role="form" class="form-horizontal" action="{{url('admin/paket/update/batch')}}" method="post">
						@forelse($paket as $k => $v)
						<div class="form-group">
					   		<div class="col-sm-offset-2 col-sm-10">
					      		<div class="checkbox">
					        		<label>
					          			<input type="checkbox" name="tempo[{{$v->id}}]" id="tempo[{{$v->id}}]" value="y" @if($v->is_active == 'y') checked="checked" @endif> {{$v->tempo}}
					        		</label>
					      		</div>
					    	</div>
					  	</div>
						@empty
						<div class="alert alert-info">
							<i class="fa fa-check"></i> Belum ada paket Grb My Car
						</div>
						@endforelse
						<div class="form-group">
							<div class="col-md-2">
								{{csrf_field()}}
							</div>
							<div class="col-md-6">
								<button type="submit" class="btn btn-success btn-flat pull-right">Simpan</button>
							</div>
						</div>

					</form>	
				</div>
			</div>





		</div>
		<div class="box-footer">
			<div class="pull-left">
				<a href="/admin/paket" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
			</div>
			<div class="pull-right">
			</div>
		</div>
	</div>

	@endsection

	@section('js_scripts')
	@endsection