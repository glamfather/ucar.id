@extends('layouts.master')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">

@endsection

@section('contentheader_title', '<i class="fa fa-gift"></i>&nbsp; Edit Paket')

@section('breadcrumb')
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#"><i class="fa fa-gift"></i> Paket</a></li>
    <li class="active">Edit paket</li>
@endsection


@section('main_content')
<div class="box">
	<div class="box-header with-border">
		Edit Paket
	</div>

	<div class="box-body">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<form role="form" class="form-horizontal" action="/admin/paket/update/{{$paket->id}}" method="post">
			<div class="form-group">
				<label for="hour" class="control-label col-md-2">Waktu</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="number" min="1" step="1" name="hour" value="{{$paket->hour}}" class="form-control" id="hour" required="required">
						<span class="input-group-addon">Jam</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="range" class="control-label col-md-2">Jarak</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="number" min="1" step="1" name="range" value="{{$paket->range}}" class="form-control" id="range" required="required">
						<span class="input-group-addon">Km</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="keterangan" class="control-label col-md-2">Keterangan</label>
				<div class="col-md-6">
					<input type="keterangan" name="keterangan" value="{{$paket->keterangan}}" class="form-control" id="keterangan" placeholder="Dalam Kota" required="required">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-2">
					{{csrf_field()}}
				</div>
				<div class="col-md-6">
            		<button type="submit" class="btn btn-success btn-flat pull-right">Simpan</button>
				</div>
			</div>

		</form>	

	</div>
	<div class="box-footer">
		<div class="pull-left">
            <a href="/admin/pakets" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>&nbsp; Back</a>
        </div>
        <div class="pull-right">
        </div>
	</div>
</div>

@endsection

@section('js_scripts')
@endsection