@extends('layouts.master')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/buttons.dataTables.min.css')}}">

@endsection

@section('contentheader_title', '<i class="fa fa-gift"></i>&nbsp; Management Harga Paket')

@section('breadcrumb')
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Harga Paket</li>
@endsection

@section('main_content')
<div class="box">
	<div class="box-header with-border">
		@if(Session::has('message'))
			<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
		@endif
	</div>
	<div class="box-body">
		<div class="box-header">
			<a href="/admin/paket/create" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp; Tambah Paket</a>
		</div>

		<table class="table table-condensed table-stripped table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th>Jenis</th>
					<th>Waktu</th>
					<th>Jarak</th>
					<th>Tempo</th>
					<th>Keterangan</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@forelse($paket as $key => $value)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{strtoupper($value->jenis)}}</td>
						<td>{{$value->jam}} @if($value->jenis == 'normal') Jam @else  - @endif</td>
						<td>{{$value->jarak}} @if($value->jenis == 'normal') Km @else  - @endif</td>
						<td>@if($value->jenis == 'grab_my_car') {{$value->tempo}} @else  - @endif</td>
						<td>{{$value->keterangan}}</td>
						<td>
							{{--<a href="/admin/paket/edit/{{$value->id}}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i>&nbsp; EDIT</a>--}}
						</td>
					</tr>
				@empty
				@endforelse
			</tbody>
		</table>

	</div>
	<div class="box-footer"></div>
</div>

@endsection

@section('js_scripts')
@endsection