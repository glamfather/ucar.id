(function($) {
    $(document).ready(function(){
		$('.same-height').responsiveEqualHeightGrid();
		
		// date picker
		$('#start-date, #start-date-airport, #start-date-taxi').datetimepicker({
			format: "D MMM YYYY",
			locale: 'id',
			ignoreReadonly: true
		});
		
		$('#date-filter').datetimepicker({
			format: "D MMM YYYY",
			locale: 'id',
			ignoreReadonly: true
		});
		
		$('#end-date, #end-date-taxi').datetimepicker({
			format: 'D MMM YYYY',
			locale: 'id',
			ignoreReadonly: true,
			useCurrent: false //Important! See issue #1075
		});
			
		$("#start-date").on("dp.change", function (e) {
			$('#end-date').data("DateTimePicker").minDate(e.date);
		});
			
		$("#start-date-taxi").on("dp.change", function (e) {
			$('#end-date-taxi').data("DateTimePicker").minDate(e.date);
		});
			
		$("#start-date-airport").on("dp.change", function (e) {
			$('#start-time-airport').focus();
		});
			
		$("#start-date").on("dp.hide", function (e) {
			$('#start-time').focus();
		});
			
		$("#start-date-taxi").on("dp.hide", function (e) {
			$('#start-time-taxi').focus();
		});
			
		$("#end-date").on("dp.change", function (e) {
			$('#start-date').data("DateTimePicker").maxDate(e.date);
		});
			
		$("#end-date-taxi").on("dp.change", function (e) {
			$('#start-date-taxi').data("DateTimePicker").maxDate(e.date);
		});
			
		$("#end-date").on("dp.hide", function (e) {
			$('#end-time').focus();
		});
			
		$("#end-date-taxi").on("dp.hide", function (e) {
			$('#end-time-taxi').focus();
		});
		
		// time picker
		$('#start-time, #start-time-airport, #start-time-taxi').datetimepicker({
			format: 'HH:mm',
			ignoreReadonly: true
		});
		
		$('#start-time').on("dp.hide", function (e) {
			$('#end-date').focus();
		});
		
		$('#start-time-taxi').on("dp.hide", function (e) {
			$('#end-date-taxi').focus();
		});
		
		$('#end-time, #end-time-taxi').datetimepicker({
			format: 'HH:mm',
			ignoreReadonly: true
		});
		

		$('#detail-date-a').datetimepicker({
			format: "D MMM YYYY",
			locale: 'id',
			inline: true,
			sideBySide: true
		});
		
		$('#detail-date-b').datetimepicker({
			format: "D MMM YYYY",
			locale: 'id',
			inline: true,
			sideBySide: true,
			useCurrent: false //Important! See issue #1075
		});
		
		$("#detail-date-a").on("dp.change", function (e) {
			$('#detail-date-b').data("DateTimePicker").minDate(e.date);
		});
		
		$("#detail-date-b").on("dp.change", function (e) {
			$('#detail-date-a').data("DateTimePicker").maxDate(e.date);
		});
		
		$('#date-pickup-a, #date-pickup-b').datetimepicker({
			format: "D MMM YYYY",
			locale: 'id',
			ignoreReadonly: true
		});
		
		$('#time-pickup-a, #time-pickup-b').datetimepicker({
			format: 'HH:mm',
			ignoreReadonly: true
		});
		
		$("#from, #from-taxi").typeahead({
			source:[
			{name: "Kemayoran - Jakarta"}, 
			{name: "Bandung Lautan Api - Bandung"}
			],
			
			autoSelect: true,
			minLength: 3
		});
		
		$("#date-pickup-a").on("dp.hide", function (e) {
			$('#time-pickup-a').focus();
		});
		
		$("#date-pickup-b").on("dp.hide", function (e) {
			$('#time-pickup-b').focus();
		});
		
		$("#from-airport, #from-airport-b").typeahead({
			source:[
			{name: "Kemayoran - Jakarta"}, 
			{name: "Bandung Lautan Api - Bandung"}
			],
			
			autoSelect: true,
			minLength: 3
		});
		
		$("#airport, #airport-b").typeahead({
			source:[
			{name: "Sultan Thaha - Jakarta"}, 
			{name: "Sutan Kasim - Jambi"}
			],
			
			autoSelect: true,
			minLength: 3
		});
		
		
		// filter_harga
		$("#filter_harga").ionRangeSlider({
			type: "double",
			grid: false,
			min: 0,
			max: 1000000,
			from: 0,
			to: 1000000,
			prefix: "Rp",
			step: 10000,
			prettify_enabled: true,
			prettify_separator: "."
		});
		
		// filter year
		$("#filter_year").ionRangeSlider({
			type: "double",
			grid: false,
			min: 2000,
			max: 2016,
			from: 0,
			to: 2016,
			prefix: "",
			step: 1,
			prettify_enabled: false,
		});
		
		$('#date-filter').on("dp.show", function (e) {
			$("#date-filter + div").addClass('bottom');
			$("#date-filter + div").removeClass('top');
		});
		
		// avoid auto slide
		$('.carousel').carousel({
			interval: false
		});
		
		$('#bdr').on('show.bs.tab', function (e) {
			$(".tab-content #bandara").parent().parent().parent().parent().addClass('respon-top');
		})
		
		$('#bdr').on('hidden.bs.tab', function (e) {
			$(".tab-content #bandara").parent().parent().parent().parent().removeClass('respon-top');
		})
		
		$('#smooth-scroll-btn').click(function() {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000); // The number here represents the speed of the scroll in milliseconds
					return false;
				}
			}
		});
	});
})(jQuery);