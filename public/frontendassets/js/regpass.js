$(function(){
	$('#form-register-password').keyup(function(){
		var data 	= zxcvbn($(this).val());
		var score 	= data.score + 1;
		score 		= (score >= 4) ? 4 : score;
		$('#password-strength-meter li').removeClass('active');

		for (var n = 1; n < (score + 1); n++){
			if($(this).val()){
				$('#password-strength-meter li:nth-child('+ n +')').addClass('active');
				(score == n)
					? $('#password-strength-meter li:nth-child('+ n +')').addClass('last')
					: $('#password-strength-meter li:nth-child('+ n +')').removeClass('last');
				(score == 1) 
					? $('#password-strength-meter li:nth-child('+ n +')').html('<span>Bad</span>')
					: (score == 2)
						?  $('#password-strength-meter li:nth-child('+ n +')').html('<span>Poor</span>')
						: (score == 3)
							? $('#password-strength-meter li:nth-child('+ n +')').html('<span>Good</span>')
							: (score == 4)
								? $('#password-strength-meter li:nth-child('+ n +')').html('<span>Strong</span>')
								: '';
			}
		};

		var password = $(this).val();
		var repassword = $('#form-register-repassword').val();
		if(password!='' || repassword!='')
		{
			if(password!=repassword)
			{
				$('.password-match').html('<i class="ic-check text-success"></i>');
			}
			else
			{
				$('.password-match').html('<i class="ic-clear text-danger"></i>');
			}
		}
		else
		{
			$('.password-match').html();
		}
	});
	
	$('#form-register-password').focusout(function(){
		var password = $('#form-register-password').val();
		var repassword = $(this).val();
		if(password!='' || repassword!='')
		{
			if(password!=repassword)
			{
				$('.password-match').html('<i class="ic-clear text-danger"></i>');
			}
			else
			{
				$('.password-match').html('<i class="ic-check text-success"></i>');
			}
		}
		else
		{
			$('.password-match').html();
		}
			
	});
	$('#form-register-repassword').focusout(function(){
		var password = $('#form-register-password').val();
		var repassword = $(this).val();
		if(password!='' || repassword!='')
		{
			if(password!=repassword)
			{
				$('.password-match').html('<i class="ic-clear text-danger"></i>');
			}
			else
			{
				$('.password-match').html('<i class="ic-check text-success"></i>');
			}
		}
		else
		{
			$('.password-match').html();
		}
			
	});
	$('#form-register-repassword').focusin(function(){
		var password = $('#form-register-password').val();
		var repassword = $(this).val();
		if(password!='' || repassword!='')
		{
			if(password!=repassword)
			{
				$('.password-match').html('<i class="ic-clear text-danger"></i>');
			}
			else
			{
				$('.password-match').html('<i class="ic-check text-success"></i>');
			}
		}
		else
		{
			$('.password-match').html();
		}
			
	});
	$('#form-register-repassword').keyup(function(){
		var password = $('#form-register-password').val();
		var repassword = $(this).val();
		if(password!='' || repassword!='')
		{
			if(password!=repassword)
			{
				$('.password-match').html('<i class="ic-clear text-danger"></i>');
			}
			else
			{
				$('.password-match').html('<i class="ic-check text-success"></i>');
			}
		}
		else
		{
			$('.password-match').html();
		}
	});

	$('#form-register-old-password').keyup(function(e){
		e.preventDefault();
		if($(this).val().length > 5)
		{
			var oldpassword = $(this).val();
			var url 		= ajax_url + 'process';
			var formData 	= 'oldpassword=' + oldpassword + '&widget_name=changepassword';

			$.ajax({
				type: 'POST',
				cache: false,
				url: url,
				data: formData,
				success: function(json) {
					// console.log(json);
					var results = json.data;
					if(results==0)
					{
						$('.old-password-match').html('<i class="ic-clear text-danger"></i>');
					}
					else
					{
						$('.old-password-match').html('<i class="ic-check text-success"></i>');
					}
				}
			});
		}
	});
});
